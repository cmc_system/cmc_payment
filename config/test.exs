use Mix.Config

config :cmc_payment,
  payment: CmcPayment.PaymentMock,
  httpoison: CmcPayment.HTTPoisonMock,
  payment_stripe: CmcPayment.Payment.StripeMock,
  stripe_plan_id: "plan_Fr9b1qI2jX1UkO",
  premia: CmcPayment.Payment.PremiaMock,
  testing: CmcPayment.TestingMock

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :cmc_payment, CmcPaymentWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :cmc_payment, CmcPayment.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "cmc_payment_test",
  hostname: "db",
  pool: Ecto.Adapters.SQL.Sandbox
