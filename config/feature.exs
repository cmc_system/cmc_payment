use Mix.Config

config :cmc_payment,
  stripe_url: "stripe_mock:8080"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :cmc_payment, CmcPaymentWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: System.get_env("LOG_LEVEL", "warn") |> String.to_atom

# Configure your database
config :cmc_payment, CmcPayment.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "cmc_payment_feature",
  hostname: "db",
  pool: Ecto.Adapters.SQL.Sandbox

{ip, _} = System.cmd("bash", ["get_ip.sh"])
ip = String.replace(ip, "\n", "")

config :hound,
  driver: "selenium",
  browser: "chrome",
  host: "http://selenium",
  port: 4444,
  app_host: "http://admin:testtest@#{ip}.nip.io",
  app_port: 4001
