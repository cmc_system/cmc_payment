# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :cmc_payment,
  ecto_repos: [CmcPayment.Repo],
  payment: CmcPayment.Payment,
  httpoison: HTTPoison,
  stripe_url: System.get_env("STRIPE_API") || "https://api.stripe.com",
  stripe_username: System.get_env("STRIPE_PRIVATE_KEY") || "sk_test_v9eM6vB3SdOWfy6JCUjB58Eh00nhKOciSf",
  stripe_password: "",
  payment_stripe: CmcPayment.Payment.Stripe,
  stripe_plan_id: System.get_env("STRIPE_PLAN") || "plan_Fr9b1qI2jX1UkO",
  basic_auth: [
    username: System.get_env("BASIC_AUTH_USERNAME") || "admin",
    password: System.get_env("BASIC_AUTH_PASSWORD") || "testtest",
  ],
  premia: CmcPayment.Payment.Premia,
  testing: CmcPayment.Testing


# Configures the endpoint
config :cmc_payment, CmcPaymentWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "g0bMZOzYDUqzfBG5TOrW2+GsWwmSY/m8kPuFOYx6q69REPg9EpedsvAtUUt3dQvb",
  render_errors: [view: CmcPaymentWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CmcPayment.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :jsonapi,
  field_transformation: :camelize

config :hound,
  driver: "chrome",
  host: "http://selenium",
  port: 4444,
  path_prefix: "wd/hub/"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
