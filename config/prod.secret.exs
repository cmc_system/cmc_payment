use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :cmc_payment, CmcPaymentWeb.Endpoint,
  secret_key_base: "s6C4adMsEzo8HCV7GmaYWiLL4guO44Tp5chJ6YGgX5U+VOo7nZozyyjSTbF2TmFd"

# Configure your database
config :cmc_payment, CmcPayment.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DATABASE_USERNAME"),
  password: System.get_env("DATABASE_PASSWORD"),
  database: System.get_env("DATABASE_DATABASE"),
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 15
