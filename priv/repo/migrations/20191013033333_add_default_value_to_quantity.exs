defmodule CmcPayment.Repo.Migrations.AddDefaultValueToQuantity do
  use Ecto.Migration

  def change do
    alter table("subscriptions") do
      modify :quantity, :integer, default: 0
    end
  end
end
