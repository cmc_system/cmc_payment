defmodule CmcPayment.Repo.Migrations.SetUniqOrganizationId do
  use Ecto.Migration

  def change do
    create unique_index(:subscriptions, [:organization_id])
  end
end
