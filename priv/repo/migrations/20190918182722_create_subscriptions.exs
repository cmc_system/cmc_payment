defmodule CmcPayment.Repo.Migrations.CreateSubscriptions do
  use Ecto.Migration

  def change do
    create table(:subscriptions) do
      add :stripe_session_id, :string, null: false
      add :organization_id, :integer, null: false
      add :email, :string, null: false
      add :quantity, :integer, null: false, dafault: 0
      add :stripe_customer_id, :string, null: true
      add :stripe_subscription_id, :string, null: true

      timestamps()
    end

  end
end
