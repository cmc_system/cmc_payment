defmodule CmcPayment.Repo.Migrations.CreateCharges do
  use Ecto.Migration

  def change do
    create table(:charges) do
      add :amount, :decimal
      add :paypal_id, :string

      timestamps()
    end

  end
end
