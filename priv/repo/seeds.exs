# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CmcPayment.Repo.insert!(%CmcPayment.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias CmcPayment.Payment
alias CmcPayment.Repo

{:ok, time} = DateTime.now("Etc/UTC")

Ecto.Adapters.SQL.query!(Repo,
  """
  INSERT INTO subscriptions (
    stripe_session_id,
    organization_id,
    email,
    stripe_customer_id,
    stripe_subscription_id,
    quantity,
    inserted_at,
    updated_at
  )
  VALUES (
    'cs_test_xpK6Pq8jMXbcxlV41ESuACxwqEosAGNM52zTSiqsp7PO1TnuJ23vCUPM',
    1,
    'test@example.com',
    'cus_FvqHIeUAq8T0Wc',
    'sub_FwZ0oDDH6KjJOZ',
    1,
    '#{time}',
    '#{time}'
  )
  """
)
