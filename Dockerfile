FROM elixir:1.9.4-alpine

WORKDIR /app
EXPOSE 4000

RUN apk add --update \
  postgresql-client nodejs nodejs npm \
  tmux bash openssh-client vim \
  inotify-tools git grep

RUN mix local.hex --force
RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez

COPY mix.exs mix.lock ./

RUN mix deps.get && mix local.rebar --force

COPY assets ./assets
RUN cd assets/ && \
    npm install && \
    npm run deploy && \
    cd -

RUN MIX_ENV=prod mix deps.compile

COPY . ./

RUN MIX_ENV=prod mix compile && MIX_ENV=dev mix compile
RUN MIX_ENV=prod mix phx.digest
RUN MIX_ENV=prod mix release

# Overwrite it on production with 
# _build/prod/rel/cmc_payment/bin/cmc_payment start
CMD mix phx.server
