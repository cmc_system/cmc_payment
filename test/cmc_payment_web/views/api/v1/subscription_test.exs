defmodule CmcPaymentWeb.Api.V1.SubscriptionTest do
  use CmcPaymentWeb.ConnCase, async: true

  test "renders the stripe_session correctly" do
    assert JSONAPI.Serializer.serialize(
      CmcPaymentWeb.Api.V1.SubscriptionView,
      %{
        id: 1,
        stripe_session_id: "stripe_session_id",
        organization_id: "organization_id",
        email: "test@example.com",
        quantity: 2
     }
    ) ==
      %{
        data: %{
          attributes: %{
            "stripeSessionId" => "stripe_session_id",
            "organizationId" => "organization_id",
            "email" => "test@example.com",
            "quantity" => 2
          },
          id: "1",
          links: %{self: "/api/v1/subscriptions/1"},
          relationships: %{},
          type: "subscriptions"
        },
        included: [],
        links: %{self: "/api/v1/subscriptions/1"}
      }
  end
end
