defmodule CmcPaymentWeb.Api.V1.StripeSessionTest do
  use CmcPaymentWeb.ConnCase, async: true

  test "renders the stripe_session correctly" do
    assert JSONAPI.Serializer.serialize(
      CmcPaymentWeb.Api.V1.StripeSessionView,
      %{id: 1}
    ) ==
      %{
        data: %{
          attributes: %{},
          id: "1",
          links: %{self: "/api/v1/stripe_session/1"},
          relationships: %{},
          type: "stripe_session"
        },
        included: [],
        links: %{self: "/api/v1/stripe_session/1"}
      }
  end
end
