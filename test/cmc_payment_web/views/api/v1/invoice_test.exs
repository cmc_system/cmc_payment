defmodule CmcPaymentWeb.Api.V1.InvoiceTest do
  use CmcPaymentWeb.ConnCase, async: true
  alias CmcPaymentWeb.Api.V1.InvoiceView
  alias CmcPayment.Payment.Invoice

  test "list subscription invoices" do
    assert JSONAPI.Serializer.serialize(
      InvoiceView,
      %Invoice{
        id: "in_1FbueoLeD4GoxaIE6rVfYns7",
        created_at: ~U[2019-10-06 19:59:04Z],
        amount_due: 100,
        amount_paid: 100,
        amount_remaining: 0,
        pdf: "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
      }
    ) ==
      %{
        data: %{
          attributes: %{
            "createdAt" => ~U[2019-10-06 19:59:04Z],
            "amountDue" => 100,
            "amountPaid" => 100,
            "amountRemaining" => 0,
            "pdf" => "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
          },
          id: "in_1FbueoLeD4GoxaIE6rVfYns7",
          links: %{self: "/api/v1/invoices/in_1FbueoLeD4GoxaIE6rVfYns7"},
          relationships: %{},
          type: "invoices"
        },
        included: [],
        links: %{self: "/api/v1/invoices/in_1FbueoLeD4GoxaIE6rVfYns7"}
      }
  end
end
