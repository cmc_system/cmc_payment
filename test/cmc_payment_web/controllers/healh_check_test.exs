defmodule CmcPaymentWeb.HealthCheckTest do
  use CmcPaymentWeb.ConnCase, async: true

  test "return 200", %{conn: conn} do
    conn = get conn, health_check_path(conn, :index)
    assert conn.status == 200 
  end
end
