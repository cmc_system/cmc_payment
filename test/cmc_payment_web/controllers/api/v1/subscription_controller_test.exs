defmodule CmcPaymentWeb.Api.V1.SubscriptionControllerTest do
  use CmcPaymentWeb.ConnCase, async: true

  alias CmcPayment.Payment
  import Mox
  setup :verify_on_exit!

  @subscription %Payment.Subscription{
    id: "1",
    stripe_session_id: "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50", 
    email: "test@example.com",
    organization_id: 1
  }

  setup %{conn: conn} do
    conn = conn
       |> put_req_header("authorization", "Basic YWRtaW46dGVzdHRlc3Q=")

    {:ok, conn: conn}
  end

  describe "subscription" do
    test "show a subscription", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn "1" -> @subscription end)

      conn = get conn, api_v1_subscription_path(conn, :show, @subscription)

      json_response(conn, :ok)
    end

    test "show a subscription by organization", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription_by_organization!, fn "1" -> @subscription end)

      conn = get conn, api_v1_organization_subscription_path(conn, :show, 1)

      json_response(conn, :ok)
    end

    test "create subscription with valid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_subscription, fn _data -> {:ok, @subscription} end)

      create_data = %{
        data: %{
          attributes: %{
            stripe_session_id: "cs_test_8fh4AlX39Ncd3y8R", 
            email: "test@example.com",
            organization_id: 1
          }
        }
      }

      conn = post conn, api_v1_subscription_path(conn, :create), create_data

      json_response(conn, :created)
    end

    test "does not create subscription with invalid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_subscription, fn _data -> {
        :error, %Ecto.Changeset{errors: [quantity: {"is invalid", []}]}
      }end)

      create_data = %{
        data: %{
          attributes: %{
            stripe_session_id: "cs_test_8fh4AlX39Ncd3y8R", 
            email: "test@example.com",
            organization_id: 1,
            quantity: -1
          }
        }
      }

      conn = post conn, api_v1_subscription_path(conn, :create), create_data

      json_response(conn, :unprocessable_entity)
    end

    test "update subscription with valid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:update_subscription, fn _id, _data -> {:ok, @subscription} end)
      |> expect(:get_subscription!, fn "1" -> @subscription end)

       update_data = %{
         data: %{
           attributes: %{
             stripe_session_id: "cs_test_8fh4AlX39Ncd3y8R", 
             quantity: 2
           }
         }
       }

      conn = patch conn, api_v1_subscription_path(conn, :update, @subscription), update_data

      json_response(conn, :ok)
    end

    test "update subscription with invalid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:update_subscription, fn _id, _data -> {
          :error, %Ecto.Changeset{errors: [quantity: {"is invalid", []}]}
        }end)
      |> expect(:get_subscription!, fn "1" -> @subscription end)

       update_data = %{
         data: %{
           attributes: %{
             quantity: -1
           }
         }
       }

      conn = patch conn, api_v1_subscription_path(conn, :update, @subscription), update_data

      json_response(conn, :unprocessable_entity)
    end

    test "update subscription with no existing subscription", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn "1" ->
        raise Ecto.NoResultsError, queryable: Payment.Subscription
      end)

       update_data = %{
         data: %{
           attributes: %{
             stripe_session_id: "cs_test_8fh4AlX39Ncd3y8R", 
             quantity: 2
           }
         }
       }

      assert_raise Ecto.NoResultsError, fn ->
        patch conn, api_v1_subscription_path(conn, :update, @subscription), update_data
      end
    end
  end
end
