defmodule CmcPaymentWeb.Api.V1.StripeSessionControllerTest do
  use CmcPaymentWeb.ConnCase, async: true

  import Mox

  setup :verify_on_exit!

  @stripe_session %{id: "testid"}
  @create_data %{
    data: %{
      attributes: %{
        success_url: "www.example.com/success_url",
        cancel_url: "www.example.com/cancel_url"
      }
    }
  }
  @invalid_create_data %{
    data: %{
      attributes: %{
        cancel_url: "www.example.com/cancel_url"
      }
    }
  }

  setup %{conn: conn} do
    conn = conn
       |> put_req_header("authorization", "Basic YWRtaW46dGVzdHRlc3Q=")

    {:ok, conn: conn}
  end

  describe "create stripe session" do
    test "create stripe session with valid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_checkout_session, fn _data -> {:ok, @stripe_session} end)

      conn = post conn, api_v1_stripe_session_path(conn, :create), @create_data

      assert conn.status == 201 
    end

    test "create stripe session with invalid data", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_checkout_session, fn _data -> {
        :error, %Ecto.Changeset{errors: [success_url: {"is invalid", []}]}
      }end)

      conn = post conn, api_v1_stripe_session_path(conn, :create), @invalid_create_data

      assert conn.status == 422
    end
  end
end
