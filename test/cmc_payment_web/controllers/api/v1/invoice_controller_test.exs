defmodule CmcPaymentWeb.Api.V1.InvoiceControllerTest do
  use CmcPaymentWeb.ConnCase, async: true

  alias CmcPayment.Payment.Subscription
  alias CmcPayment.Payment.Invoice
  import Mox

  setup :verify_on_exit!

  @stripe_session %{id: "testid"}
  @create_data %{
    data: %{
      attributes: %{
        success_url: "www.example.com/success_url",
        cancel_url: "www.example.com/cancel_url"
      }
    }
  }
  @invalid_create_data %{
    data: %{
      attributes: %{
        cancel_url: "www.example.com/cancel_url"
      }
    }
  }

  setup %{conn: conn} do
    conn = conn
       |> put_req_header("authorization", "Basic YWRtaW46dGVzdHRlc3Q=")

    {:ok, conn: conn}
  end

  test "list subscription invoices", %{conn: conn} do
    subscription = %Subscription{
      id: 1,
      stripe_customer_id: "cus_FvqHIeUAq8T0Wc"
    }
    CmcPayment.PaymentMock
    |> expect(:get_subscription!, fn "1" -> subscription end)
    |> expect(:list_subscription_invoices!, fn subscription -> [%Invoice{
      id: 1
    }]end)

    conn = get conn, api_v1_subscription_invoice_path(conn, :index, subscription)

    assert conn.status == 200 
  end

  test "get organizations/<id>/invoices", %{conn: conn} do
    subscription = %Subscription{
      id: 1,
      stripe_customer_id: "cus_FvqHIeUAq8T0Wc"
    }
    CmcPayment.PaymentMock
    |> expect(:get_subscription_by_organization!, fn "1" -> @subscription end)
    |> expect(:list_subscription_invoices!, fn subscription -> [%Invoice{
      id: 1
    }]end)

    conn = get conn, api_v1_organization_invoice_path(conn, :index, 1)

    assert conn.status == 200 
  end
end
