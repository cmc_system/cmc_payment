defmodule CmcPaymentWeb.Api.V1.TestingControllerTest do
  use CmcPaymentWeb.ConnCase, async: true

  import Mox

  setup :verify_on_exit!

  setup %{conn: conn} do
    conn = conn
       |> put_req_header("authorization",
            "Basic YWRtaW46dGVzdHRlc3Q=")

    {:ok, conn: conn}
  end

  test "POST /api/v1/testing", %{conn: conn} do
    CmcPayment.TestingMock
    |> expect(:create!, fn -> nil end)

    conn = post conn, api_v1_testing_path(conn, :create)

    assert conn.status == 201
  end

  test "DELETE /api/v1/testing", %{conn: conn} do
    CmcPayment.TestingMock
    |> expect(:delete_all!, fn -> nil end)

    conn = delete conn, api_v1_testing_path(conn, :delete)

    assert conn.status == 200 
  end
end
