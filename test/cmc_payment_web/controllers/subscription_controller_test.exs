defmodule CmcPaymentWeb.SubscriptionControllerTest do
  use CmcPaymentWeb.ConnCase, async: true

  alias CmcPayment.Payment
  import Mox
  setup :verify_on_exit!

  @subscription %Payment.Subscription{
    id: 1,
    stripe_session_id: "some stripe_id",
    organization_id: 1,
    email: "test@example.com",
    premia: 0
  }
  @create_attrs %{
    stripe_session_id: "some stripe_id",
    organization_id: 1,
    email: "test@example.com"
  }
  @update_attrs %{stripe_session_id: "some updated stripe_id"}
  @invalid_attrs %{stripe_session_id: nil}

  setup %{conn: conn} do
    conn = conn
       |> put_req_header("authorization", "Basic YWRtaW46dGVzdHRlc3Q=")

    {:ok, conn: conn}
  end

  describe "index" do
    test "lists all subscriptions", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:list_subscriptions, fn -> [@subscription] end)

      conn = get conn, subscription_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Subscriptions"
    end
  end

  describe "show" do
    test "show a subscriptions", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn _ -> @subscription end)
      |> expect(:list_subscription_invoices!, fn _ -> [%CmcPayment.Payment.Invoice{
        id: "in_1FbueoLeD4GoxaIE6rVfYns7",
        amount_due: 100,
        amount_paid: 100,
        amount_remaining: 0
      }]end)

      conn = get conn, subscription_path(conn, :show, @subscription)
      assert html_response(conn, 200) =~ "Show Subscription"
      assert html_response(conn, 200) =~ "amount_paid"
      assert html_response(conn, 200) =~ "100"
    end
  end

  describe "new subscription" do
    test "renders form", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_subscription_changeset, fn ->
        %Ecto.Changeset{data: %Payment.Subscription{}}
      end)

      conn = get conn, subscription_path(conn, :new)
      assert html_response(conn, 200) =~ "New Subscription"
      assert html_response(conn, 200) =~ "subscription[stripe_session_id]"
      assert html_response(conn, 200) =~ "subscription[organization_id]"
      assert html_response(conn, 200) =~ "subscription[email]"
      assert html_response(conn, 200) =~ "subscription[quantity]"
    end
  end

  describe "create subscription" do
    test "redirects to show when data is valid", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_subscription, fn _ -> {:ok, @subscription} end)
      |> expect(:get_subscription!, fn _ -> @subscription end)
      |> expect(:list_subscription_invoices!, fn _ -> [] end)

      original_conn = conn
      conn = post conn, subscription_path(conn, :create), subscription: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == subscription_path(conn, :show, id)

      conn = get original_conn, subscription_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Subscription"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:create_subscription, fn _ ->
        {
          :error,
          %Ecto.Changeset{
            errors: [stripe_session_id: {"is invalid", []}],
            data: struct(Payment.Subscription, @invalid_attrs),
            action: :insert
          }
        }
      end)

      conn = post conn, subscription_path(conn, :create), subscription: @invalid_attrs
      assert html_response(conn, 200) =~ "New Subscription"
      assert html_response(conn, 200) =~ "Oops, something went wrong! Please check the errors below."
    end
  end

  describe "edit subscription" do
    test "renders form for editing chosen subscription", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn _ -> @subscription end)
      |> expect(:change_subscription, fn _ ->
        %Ecto.Changeset{data: @subscription}
      end)

      conn = get conn, subscription_path(conn, :edit, @subscription)
      assert html_response(conn, 200) =~ "Edit Subscription"
    end
  end

  describe "update subscription" do
    test "redirects when data is valid", %{conn: conn} do
      updated_subscription = Map.merge(@subscription, @update_attrs)
      original_conn = conn
      CmcPayment.PaymentMock
      |> expect(:update_subscription, fn _, _ -> {:ok, updated_subscription}end)
      |> expect(:get_subscription!, fn _ -> @subscription end)
      |> expect(:list_subscription_invoices!, fn _ -> [] end)

      conn = put conn, subscription_path(conn, :update, @subscription), subscription: @update_attrs
      assert redirected_to(conn) == subscription_path(conn, :show, updated_subscription)

      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn _ -> updated_subscription end)

      conn = get original_conn, subscription_path(conn, :show, updated_subscription)
      assert html_response(conn, 200) =~ "some updated stripe_id"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:update_subscription, fn _, _ ->
        {
          :error,
          %Ecto.Changeset{
            errors: [stripe_session_id: {"is invalid", []}],
            data: @subscription
          }
        }
      end)
      |> expect(:get_subscription!, fn _ -> @subscription end)

      conn = put conn, subscription_path(conn, :update, @subscription), subscription: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Subscription"
    end
  end

  describe "delete subscription" do
    test "deletes chosen subscription", %{conn: conn} do
      CmcPayment.PaymentMock
      |> expect(:get_subscription!, fn _ -> @subscription end)
      |> expect(:delete_subscription, fn _ -> {:ok, @subscription} end)
      |> expect(:get_subscription!, fn _ ->
        raise Ecto.NoResultsError, queryable: Payment.Subscription
      end)

      original_conn = conn

      conn = delete conn, subscription_path(conn, :delete, @subscription)
      assert redirected_to(conn) == subscription_path(conn, :index)
      assert_error_sent 404, fn ->
        get original_conn, subscription_path(conn, :show, @subscription)
      end
    end
  end
end
