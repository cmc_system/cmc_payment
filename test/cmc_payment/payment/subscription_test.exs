defmodule CmcPayment.Payment.SubscriptionTest do
  use CmcPayment.DataCase
  alias CmcPayment.Payment.Subscription

  describe "create_changeset/1" do
    @valid_attr %{
      stripe_session_id: "some stripe_id",
      organization_id: 1,
      email: "test@example.com",
      quantity: 1
    }

    test "return a valid changeset when all data is valid" do
      %Ecto.Changeset{valid?: true} = Subscription.create_changeset(@valid_attr)
    end

    test "return an invalid changeset when stripe_session_id is not present" do
      invalid_attr = Map.delete(@valid_attr, :stripe_session_id)
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(invalid_attr)
    end

    test "return an invalid changeset when organization_id is not present" do
      invalid_attr = Map.delete(@valid_attr, :organization_id)
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(invalid_attr)
    end

    test "return an invalid changeset when email is not present" do
      invalid_attr = Map.delete(@valid_attr, :email)
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(invalid_attr)
    end

    test "return an valid changeset when quantity is not present" do
      valid_attr = Map.delete(@valid_attr, :quantity)
      %Ecto.Changeset{valid?: true} = Subscription.create_changeset(valid_attr)
    end

    test "return an invalid changeset when quantity == nil" do
      invalid_attr = Map.put(@valid_attr, :quantity, nil)
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(invalid_attr)
    end

    test "return an invalid changeset when quantity < 0" do
      invalid_attr = Map.put(@valid_attr, :quantity, -1)
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(invalid_attr)
    end

    test "return an valid changeset when quantity == 0" do
      valid_attr = Map.put(@valid_attr, :quantity, 0)
      %Ecto.Changeset{valid?: true} = Subscription.create_changeset(valid_attr)
    end

    test "return an invalid changeset when organization_id is not uniq" do
      %CmcPayment.Payment.Subscription{}
      |> Map.merge(@valid_attr)
      |> Repo.insert!
      %Ecto.Changeset{valid?: false} = Subscription.create_changeset(@valid_attr)
    end
  end

  describe "update_changeset/2" do
    @valid_attr %{
      stripe_session_id: "new stripe_id",
      email: "new@example.com",
      quantity: 2
    }
    @subscription %Subscription{
      stripe_session_id: "some stripe_id",
      email: "test@example.com",
      quantity: 1,
      organization_id: 1
    }

    test "return a valid changeset when all data is valid" do
      %Ecto.Changeset{valid?: true} =
        Subscription.update_changeset(@subscription, @valid_attr)
    end

    test "return an invalid changeset when stripe_session_id is not present" do
      valid_attr = Map.delete(@valid_attr, :stripe_session_id)
      %Ecto.Changeset{valid?: true} =
        Subscription.update_changeset(@subscription, valid_attr)
    end

    test "return an invalid changeset when stripe_session_id == nil" do
      invalid_attr = Map.put(@valid_attr, :stripe_session_id, nil)
      %Ecto.Changeset{valid?: false} =
        Subscription.update_changeset(@subscription, invalid_attr)
    end

    test "return an invalid changeset when email == nil" do
      invalid_attr = Map.put(@valid_attr, :email, nil)
      %Ecto.Changeset{valid?: false} =
        Subscription.update_changeset(@subscription, invalid_attr)
    end

    test "return an valid changeset when quantity is not present" do
      valid_attr = Map.delete(@valid_attr, :quantity)
      %Ecto.Changeset{valid?: true} =
        Subscription.update_changeset(@subscription, valid_attr)
    end

    test "return an invalid changeset when quantity == nil" do
      invalid_attr = Map.put(@valid_attr, :quantity, nil)
      %Ecto.Changeset{valid?: false} =
        Subscription.update_changeset(@subscription, invalid_attr)
    end

    test "return an invalid changeset when quantity < 0" do
      invalid_attr = Map.put(@valid_attr, :quantity, -1)
      %Ecto.Changeset{valid?: false} =
        Subscription.update_changeset(@subscription, invalid_attr)
    end

    test "return an valid changeset when quantity == 0" do
      invalid_attr = Map.put(@valid_attr, :quantity, 0)
      %Ecto.Changeset{valid?: true} =
        Subscription.update_changeset(@subscription, invalid_attr)
    end
  end
end
