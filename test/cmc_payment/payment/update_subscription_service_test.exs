defmodule CmcPayment.Payment.UpdateSubscriptionServiceTest do
  use CmcPayment.DataCase
  alias CmcPayment.Payment.UpdateSubscriptionService
  alias CmcPayment.Payment.Subscription

  import Mox

  test "update_subscription/1 with valid data creates a subscription" do
    mock_stripe()
    valid_attrs = %{
      stripe_session_id: "some stripe_id",
      organization_id: 1,
      email: "test@example.com",
      quantity: 2
    }
    subscription = subscription_fixture()

    assert {:ok, %Subscription{} = updated_subscription}
      = UpdateSubscriptionService.update_subscription(subscription, valid_attrs)
    assert updated_subscription.stripe_session_id == "some stripe_id"
    assert updated_subscription.email == "test@example.com"
    assert updated_subscription.quantity == 2
  end

  test "update_subscription/2 with invalid does not creates a subscription" do
    invalid_attrs = %{quantity: nil}
    subscription = subscription_fixture()

    assert {:error, %Ecto.Changeset{} = changeset}
      = UpdateSubscriptionService.update_subscription(subscription, invalid_attrs)
    assert changeset.valid? == false
  end

  @valid_attrs %Subscription{
    stripe_session_id: "some stripe_id",
    stripe_customer_id: "cus_FrJAjCNtzbcjgo",
    stripe_subscription_id: "sub_Fr9gtlEuoJ9vtL",
    organization_id: 1,
    email: "test@example.com",
    quantity: 1
  }

  def subscription_fixture() do
    {:ok, subscription} = Repo.insert(@valid_attrs)

    subscription
  end

  def mock_stripe do
    CmcPayment.Payment.StripeMock
    |> expect(:get_checkout_session, fn "some stripe_id" ->
         {
           :ok,
           %{"setup_intent" => "seti_1FLZxlLeD4GoxaIEUSek4Ib9"}
         }
       end)
    |> expect(:get_setup_intent, fn "seti_1FLZxlLeD4GoxaIEUSek4Ib9" ->
         {
           :ok,
           %{"payment_method" => "pm_1FLa6VLeD4GoxaIEJ6cmtEiD"}
         }
       end) 
    |> expect(:create_customer, fn %{email: "test@example.com"} ->
         {
           :ok,
           %{"id" => "cus_FrJAjCNtzbcjgo"}
         }
       end) 
    |> expect(:update_customer, fn(
         "cus_FrJAjCNtzbcjgo",
         %{default_payment_method: "pm_1FLa6VLeD4GoxaIEJ6cmtEiD"}
       ) ->
         {
           :ok,
           %{"id" => "cus_FrJAjCNtzbcjgo"}
         }
       end) 
    |> expect(:attach_payment_to_customer, fn _attrs ->
         {
           :ok,
           %{}
         }
       end) 
    |> expect(:update_subscription, fn "sub_Fr9gtlEuoJ9vtL", %{
         quantity: 2
       } ->
      {
        :ok,
        %{"id" => "sub_Fr9gtlEuoJ9vtL"}
      }
    end) 
  end
end
