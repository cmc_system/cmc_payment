defmodule CmcPayment.Payment.PremiaTest do
  use CmcPayment.DataCase, async: true

  alias CmcPayment.Payment.Premia
  import Mox

  test "return 0 when quantity==0" do
    assert Premia.calcule(%{quantity: 0}) == 0 
  end

  test "return 0 when quantity==5" do
    assert Premia.calcule(%{quantity: 5}) == 0 
  end

  test "return 5 when quantity==6" do
    assert Premia.calcule(%{quantity: 6}) == 5
  end

  test "return 5 when quantity==10" do
    assert Premia.calcule(%{quantity: 10}) == 25 
  end
end
