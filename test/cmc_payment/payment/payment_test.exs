defmodule CmcPayment.PaymentTest do
  use CmcPayment.DataCase, async: true

  alias CmcPayment.Payment
  import Mox

  describe "create_checkout_session/1" do
    test "returns {:ok, data} when attrs are valid" do
      CmcPayment.Payment.StripeMock
      |> expect(:create_checkout_session, fn _attrs -> {:ok, %{id: "id"}}  end)
      valid_attrs = %{
        success_url: "www.example.com/success",
        cancel_url: "www.example.com/cancel"
      }

      assert {:ok, %{id: "id"}} == Payment.create_checkout_session(valid_attrs)
    end

    test "returns {:error, changeset} when attrs are invalid" do
      invalid_attrs = %{}

      assert {:error, %Ecto.Changeset{}} = Payment.create_checkout_session(invalid_attrs)
    end

    test "returns {:errors, changeset} when api return errors" do
      error = %{
        "error" => %{
          "code" => "url_invalid",
          "doc_url" => "https://stripe.com/docs/error-codes/url-invalid",
          "message" => "Invalid URL.",
        }
      }
      invalid_attrs = %{
        success_url: "www.example.com/success",
        cancel_url: "www.example.com/cancel"
      }
      CmcPayment.Payment.StripeMock
      |> expect(:create_checkout_session, fn _attrs -> {:ok, error} end)

      assert {:error, %{errors: [base: {"Invalid URL.", []}]}} = Payment.create_checkout_session(invalid_attrs)
    end
  end

  describe "subscriptions" do
    alias CmcPayment.Payment.Subscription

    @valid_attrs %CmcPayment.Payment.Subscription{
      stripe_session_id: "some stripe_id",
      stripe_subscription_id: "sub_FwZ0oDDH6KjJOZ",
      organization_id: 1,
      email: "test@example.com",
      quantity: 1
    }
    @update_attrs %{email: "new@example.com"}
    @invalid_attrs %{stripe_session_id: nil}

    def subscription_fixture(attrs \\ %{}) do
      {:ok, subscription} = Repo.insert(@valid_attrs)

      subscription
    end

    test "list_subscriptions/0 returns all subscriptions" do
      subscription = subscription_fixture()
      assert Payment.list_subscriptions() == [subscription]
    end

    test "get_subscription!/1 returns the subscription with given id" do
      message = File.read!("test/support/messages/subscriptions/sub_FwZ0oDDH6KjJOZ.json")
      CmcPayment.Payment.StripeMock
      |> expect(:get_subscription, fn "sub_FwZ0oDDH6KjJOZ" -> {:ok, Poison.decode!(message)} end)
      CmcPayment.Payment.PremiaMock
      |> expect(:calcule, fn %{quantity: 1} -> 0  end)

      subscription =
        subscription_fixture()
        |> Map.put(:iban_last4, "4242")
        |> Map.put(:premia, 0)

      assert Payment.get_subscription!(subscription.id) == subscription
    end

    test "get_subscription_by_organization!/1 returns the subscription for an organization" do
      message = File.read!("test/support/messages/subscriptions/sub_FwZ0oDDH6KjJOZ.json")
      CmcPayment.Payment.StripeMock
      |> expect(:get_subscription, fn "sub_FwZ0oDDH6KjJOZ" -> {:ok, Poison.decode!(message)} end)
      CmcPayment.Payment.PremiaMock
      |> expect(:calcule, fn %{quantity: 1} -> 0  end)

      subscription =
        subscription_fixture()
        |> Map.put(:iban_last4, "4242") 
        |> Map.put(:premia, 0) 
      assert Payment.get_subscription_by_organization!(subscription.organization_id) == 
        subscription
    end

    # test "create_subscription/1 with invalid data returns error changeset" do
    #   assert {:error, %Ecto.Changeset{}} = Payment.create_subscription(@invalid_attrs)
    # end

    test "delete_subscription/1 deletes the subscription" do
      subscription = subscription_fixture()
      assert {:ok, %Subscription{}} = Payment.delete_subscription(subscription)
      assert_raise Ecto.NoResultsError, fn -> Payment.get_subscription!(subscription.id) end
    end

    test "change_subscription/1 returns a subscription changeset" do
      subscription = subscription_fixture()
      assert %Ecto.Changeset{} = Payment.change_subscription(subscription)
    end

    test "create_subscription_changeset/1 returns a subscription changeset" do
      assert %Ecto.Changeset{} = Payment.create_subscription_changeset()
    end
  end

  describe "invoices" do
    test "list_customer_invoices/1 returns all customer invoices" do
      subscription = %CmcPayment.Payment.Subscription{
        id: 1,
        stripe_customer_id: "cus_FvqHIeUAq8T0Wc"
      }
      CmcPayment.Payment.StripeMock
      |> expect(:list_customer_invoices, fn "cus_FvqHIeUAq8T0Wc" -> {
        :ok, 
        %{
          "data" => [%{
            "id" => "in_1FbueoLeD4GoxaIE6rVfYns7",
            "amount_due" => 100,
            "amount_paid" => 100,
            "amount_remaining" => 0,
            "created" => 1_570_391_944,
            "invoice_pdf" => "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
          }]
        }
      } end)
      result = %CmcPayment.Payment.Invoice{
        id: "in_1FbueoLeD4GoxaIE6rVfYns7",
        amount_due: 100,
        amount_paid: 100,
        amount_remaining: 0,
        created_at: ~U[2019-10-06 19:59:04Z],
        pdf: "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
      }

      assert [result] == Payment.list_subscription_invoices!(subscription)
    end
  end

  describe "charges" do
    alias CmcPayment.Payment.Charge

    @valid_attrs %{amount: "120.5"}
    @update_attrs %{amount: "456.7"}
    @invalid_attrs %{amount: nil}

    def charge_fixture(attrs \\ %{}) do
      {:ok, charge} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Payment.create_charge()

      charge
    end

    test "list_charges/0 returns all charges" do
      charge = charge_fixture()
      assert Payment.list_charges() == [charge]
    end

    test "get_charge!/1 returns the charge with given id" do
      charge = charge_fixture()
      assert Payment.get_charge!(charge.id) == charge
    end

    test "create_charge/1 with valid data creates a charge" do
      assert {:ok, %Charge{} = charge} = Payment.create_charge(@valid_attrs)
      assert charge.amount == Decimal.new("120.5")
    end

    test "create_charge/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Payment.create_charge(@invalid_attrs)
    end

    test "update_charge/2 with valid data updates the charge" do
      charge = charge_fixture()
      assert {:ok, charge} = Payment.update_charge(charge, @update_attrs)
      assert %Charge{} = charge
      assert charge.amount == Decimal.new("456.7")
    end

    test "update_charge/2 with invalid data returns error changeset" do
      charge = charge_fixture()
      assert {:error, %Ecto.Changeset{}} = Payment.update_charge(charge, @invalid_attrs)
      assert charge == Payment.get_charge!(charge.id)
    end

    test "delete_charge/1 deletes the charge" do
      charge = charge_fixture()
      assert {:ok, %Charge{}} = Payment.delete_charge(charge)
      assert_raise Ecto.NoResultsError, fn -> Payment.get_charge!(charge.id) end
    end

    test "change_charge/1 returns a charge changeset" do
      charge = charge_fixture()
      assert %Ecto.Changeset{} = Payment.change_charge(charge)
    end
  end
end
