defmodule CmcPayment.Payment.StripeTest do
  use CmcPayment.DataCase, async: true

  alias CmcPayment.Payment
  import Mox

  test "create_checkout_session/1 create and return a stripe session" do
    response = %{
      "id" => "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50",
      "object" => "checkout.session",
      "billing_address_collection" => nil,
      "cancel_url" => "https://example.com/cancel",
      "client_reference_id" => nil,
      "customer" => nil,
      "customer_email" => nil,
      "display_items" => nil,
      "livemode" => false,
      "locale" => nil,
      "mode" => "setup",
      "payment_intent" => nil,
      "payment_method_types" => ["card"],
      "setup_intent" => "seti_1FNiKyLeD4GoxaIEPGMBU8R6",
      "submit_type" => nil,
      "subscription" => nil,
      "success_url" => "https://example.com/success?session_id={CHECKOUT_SESSION_ID}"
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, _data, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.create_checkout_session(%{
      cancel_url: "https://example.com/cancel",
      success_url: "https://example.com/success?session_id={CHECKOUT_SESSION_ID}"
    }) == {:ok, response}
  end

  test "get_checkout_session/1 returns stripe checkout session" do
    response = %{
      "id" => "cs_test_o0d1VbsFMKWHZ1x8PF5lsGaqqvacbvnrnoQ0qWPL8JNjvqW4JdDuokN2",
      "object" => "checkout.session",
      "billing_address_collection" => nil,
      "cancel_url" => "https://example.com/cancel",
      "client_reference_id" => nil,
      "customer" => nil,
      "customer_email" => nil,
      "display_items" => nil,
      "livemode" => false,
      "locale" => nil,
      "mode" => "setup",
      "payment_intent" => nil,
      "payment_method_types" => [
        "card"
      ],
      "setup_intent" => "seti_1FLZxlLeD4GoxaIEUSek4Ib9",
      "submit_type" => nil,
      "subscription" => nil,
      "success_url" => "https://example.com/success?session_id={CHECKOUT_SESSION_ID}"
    }

    CmcPayment.HTTPoisonMock
    |> expect(:get, fn _path, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.get_checkout_session("cs_test_o0d1VbsFMKWHZ1x8PF5lsGaqqvacbvnrnoQ0qWPL8JNjvqW4JdDuokN2") == 
      {:ok, response}
  end

  test "get_setup_intent/1 returns a stripe setup intent" do
    response = %{
      "id" => "seti_1FLZxlLeD4GoxaIEUSek4Ib9",
      "object" => "setup_intent",
      "application" => nil,
      "cancellation_reason" => nil,
      "client_secret" => "seti_1FLZxlLeD4GoxaIEUSek4Ib9_secret_FrIY2yMzvbyzUCMqRYN440qqPrXRHFI",
      "created" => 1_569_177_553,
      "customer" => nil,
      "description" => nil,
      "last_setup_error" => nil,
      "livemode" => false,
      "metadata" => %{
      },
      "next_action" => nil,
      "on_behalf_of" => nil,
      "payment_method" => "pm_1FLa6VLeD4GoxaIEJ6cmtEiD",
      "payment_method_options" => %{
        "card" => %{
          "request_three_d_secure" => "automatic"
        }
      },
      "payment_method_types" => [
        "card"
      ],
      "status" => "succeeded",
      "usage" => "off_session"
    }

    CmcPayment.HTTPoisonMock
    |> expect(:get, fn _path, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.get_setup_intent("seti_1FLZxlLeD4GoxaIEUSek4Ib9") == 
      {:ok, response}
  end

  test "create_customer/1 create and returns a stripe customer" do
    response = %{
      "id" => "cus_FrJAjCNtzbcjgo",
      "object" => "customer",
      "account_balance" => 0,
      "address" => nil,
      "balance" => 0,
      "created" => 1_569_179_806,
      "currency" => nil,
      "default_source" => nil,
      "delinquent" => false,
      "description" => nil,
      "discount" => nil,
      "email" => "test@example.com",
      "invoice_prefix" => "C05638D0",
      "invoice_settings" => %{
        "custom_fields" => nil,
        "default_payment_method" => nil,
        "footer" => nil
      },
      "livemode" => false,
      "metadata" => %{
      },
      "name" => nil,
      "phone" => nil,
      "preferred_locales" => [
      ],
      "shipping" => nil,
      "sources" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/sources"
      },
      "subscriptions" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/subscriptions"
      },
      "tax_exempt" => "none",
      "tax_ids" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/tax_ids"
      },
      "tax_info" => nil,
      "tax_info_verification" => nil
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, _data, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.create_customer(%{email: "test@example.com"}) == 
      {:ok, response}
  end

  test "update_customer/1 create and returns a stripe customer" do
    response = %{
      "id" => "cus_FrJAjCNtzbcjgo",
      "object" => "customer",
      "account_balance" => 0,
      "address" => nil,
      "balance" => 0,
      "created" => 1_569_179_806,
      "currency" => nil,
      "default_source" => nil,
      "delinquent" => false,
      "description" => nil,
      "discount" => nil,
      "email" => "test@example.com",
      "invoice_prefix" => "C05638D0",
      "invoice_settings" => %{
        "custom_fields" => nil,
        "default_payment_method" => 'pm_1FPyd8LeD4GoxaIEqZZN4d6Y',
        "footer" => nil
      },
      "livemode" => false,
      "metadata" => %{
      },
      "name" => nil,
      "phone" => nil,
      "preferred_locales" => [
      ],
      "shipping" => nil,
      "sources" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/sources"
      },
      "subscriptions" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/subscriptions"
      },
      "tax_exempt" => "none",
      "tax_ids" => %{
        "object" => "list",
        "data" => [
        ],
        "has_more" => false,
        "total_count" => 0,
        "url" => "/v1/customers/cus_FrJAjCNtzbcjgo/tax_ids"
      },
      "tax_info" => nil,
      "tax_info_verification" => nil
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, _data, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.update_customer("pm_1FLa6VLeD4GoxaIEJ6cmtEiD", %{
      default_payment_method: "pm_1FPyd8LeD4GoxaIEqZZN4d6Y"
    }) == 
      {:ok, response}
  end
  test "attach_payment_to_customer/1 add a payment method to a customer" do
    response = %{
      "id" => "pm_1FLa6VLeD4GoxaIEJ6cmtEiD",
      "object" => "payment_method",
      "billing_details" => %{
        "address" => %{
          "city" => nil,
          "country" => "DE",
          "line1" => nil,
          "line2" => nil,
          "postal_code" => nil,
          "state" => nil
        },
        "email" => "aronwolf90@gmail.com",
        "name" => "Aron Wolf",
        "phone" => nil
      },
      "card" => %{
        "brand" => "visa",
        "checks" => %{
          "address_line1_check" => nil,
          "address_postal_code_check" => nil,
          "cvc_check" => "pass"
        },
        "country" => "US",
        "exp_month" => 1,
        "exp_year" => 2033,
        "fingerprint" => "X79tSfMZyLyOMAAG",
        "funding" => "credit",
        "generated_from" => nil,
        "last4" => "4242",
        "three_d_secure_usage" => %{
          "supported" => true
        },
        "wallet" => nil
      },
      "created" => 1_569_178_095,
      "customer" => "cus_FrJAjCNtzbcjgo",
      "livemode" => false,
      "metadata" => %{
      },
      "type" => "card"
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, _data, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.attach_payment_to_customer(%{
      customer: "cus_FrJAjCNtzbcjgo",
      payment_method: "pm_1FLa6VLeD4GoxaIEJ6cmtEiD"
    }) == 
      {:ok, response}
  end

  test "get_subscription/1 return a subscription structure" do
    message = File.read!("test/support/messages/subscriptions/sub_FwZ0oDDH6KjJOZ.json")
    url = "https://api.stripe.com/v1/subscriptions/sub_FwZ0oDDH6KjJOZ?expand[]=customer.invoice_settings.default_payment_method"

    CmcPayment.HTTPoisonMock
    |> expect(:get, fn url, _header ->
      {
        :ok,
        %HTTPoison.Response{body: message}
      }
    end)

    assert Payment.Stripe.get_subscription("sub_Fr9gtlEuoJ9vtL") == 
      {:ok, Poison.decode!(message)}
  end

  test "create_subscription/1 create a stripe subscription" do
    response = %{
      "id" => "sub_Fr9gtlEuoJ9vtL",
      "object" => "subscription",
      "application_fee_percent" => nil,
      "billing" => "charge_automatically",
      "billing_cycle_anchor" => 1_569_144_514,
      "billing_thresholds" => nil,
      "cancel_at" => nil,
      "cancel_at_period_end" => false,
      "canceled_at" => nil,
      "collection_method" => "charge_automatically",
      "created" => 1_569_144_514,
      "current_period_end" => 1_571_736_514,
      "current_period_start" => 1_569_144_514,
      "customer" => "cus_Fr9AlEgktYlBlf",
      "days_until_due" => nil,
      "default_payment_method" => nil,
      "default_source" => nil,
      "default_tax_rates" => [
      ],
      "discount" => nil,
      "ended_at" => nil,
      "items" => %{
        "object" => "list",
        "data" => [
          %{
            "id" => "si_Fr9guQeG1Adjiq",
            "object" => "subscription_item",
            "billing_thresholds" => nil,
            "created" => 1_569_144_514,
            "metadata" => %{
            },
            "plan" => %{
              "id" => "plan_Fr9b1qI2jX1UkO",
              "object" => "plan",
              "active" => true,
              "aggregate_usage" => nil,
              "amount" => 10_000,
              "amount_decimal" => "10000",
              "billing_scheme" => "per_unit",
              "created" => 1_569_144_255,
              "currency" => "eur",
              "interval" => "month",
              "interval_count" => 1,
              "livemode" => false,
              "metadata" => %{
              },
              "nickname" => "SaaS Platform USD",
              "product" => "prod_Fr9YJzKxHAdARJ",
              "tiers" => nil,
              "tiers_mode" => nil,
              "transform_usage" => nil,
              "trial_period_days" => nil,
              "usage_type" => "licensed"
            },
            "quantity" => 1,
            "subscription" => "sub_Fr9gtlEuoJ9vtL",
            "tax_rates" => [
            ]
          }
        ],
        "has_more" => false,
        "total_count" => 1,
        "url" => "/v1/subscription_items?subscription=sub_Fr9gtlEuoJ9vtL"
      },
      "latest_invoice" => "in_1FLRMsLeD4GoxaIEu9jnciXP",
      "livemode" => false,
      "metadata" => %{
      },
      "pending_setup_intent" => nil,
      "plan" => %{
        "id" => "plan_Fr9b1qI2jX1UkO",
        "object" => "plan",
        "active" => true,
        "aggregate_usage" => nil,
        "amount" => 10_000,
        "amount_decimal" => "10000",
        "billing_scheme" => "per_unit",
        "created" => 1_569_144_255,
        "currency" => "eur",
        "interval" => "month",
        "interval_count" => 1,
        "livemode" => false,
        "metadata" => %{
        },
        "nickname" => "SaaS Platform USD",
        "product" => "prod_Fr9YJzKxHAdARJ",
        "tiers" => nil,
        "tiers_mode" => nil,
        "transform_usage" => nil,
        "trial_period_days" => nil,
        "usage_type" => "licensed"
      },
      "quantity" => 1,
      "schedule" => nil,
      "start" => 1_569_144_514,
      "start_date" => 1_569_144_514,
      "status" => "active",
      "tax_percent" => nil,
      "trial_end" => nil,
      "trial_start" => nil
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, data, _header ->
      assert data == "coupon=free-amount&customer=cus_FrJAjCNtzbcjgo&items%5B0%5D%5Bplan%5D=plan_Fr9b1qI2jX1UkO"
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.create_subscription(%{
      customer: "cus_FrJAjCNtzbcjgo",
      plan: "plan_Fr9b1qI2jX1UkO"
    }) == 
      {:ok, response}
  end

  test "update_subscription/1 create a stripe subscription" do
    response = %{
      "id" => "sub_Fr9gtlEuoJ9vtL",
      "object" => "subscription",
      "application_fee_percent" => nil,
      "billing" => "charge_automatically",
      "billing_cycle_anchor" => 1_569_144_514,
      "billing_thresholds" => nil,
      "cancel_at" => nil,
      "cancel_at_period_end" => false,
      "canceled_at" => nil,
      "collection_method" => "charge_automatically",
      "created" => 1_569_144_514,
      "current_period_end" => 1_571_736_514,
      "current_period_start" => 1_569_144_514,
      "customer" => "cus_Fr9AlEgktYlBlf",
      "days_until_due" => nil,
      "default_payment_method" => nil,
      "default_source" => nil,
      "default_tax_rates" => [
      ],
      "discount" => nil,
      "ended_at" => nil,
      "items" => %{
        "object" => "list",
        "data" => [
          %{
            "id" => "si_Fr9guQeG1Adjiq",
            "object" => "subscription_item",
            "billing_thresholds" => nil,
            "created" => 1_569_144_514,
            "metadata" => %{
            },
            "plan" => %{
              "id" => "plan_Fr9b1qI2jX1UkO",
              "object" => "plan",
              "active" => true,
              "aggregate_usage" => nil,
              "amount" => 10_000,
              "amount_decimal" => "10000",
              "billing_scheme" => "per_unit",
              "created" => 1_569_144_255,
              "currency" => "eur",
              "interval" => "month",
              "interval_count" => 1,
              "livemode" => false,
              "metadata" => %{
              },
              "nickname" => "SaaS Platform USD",
              "product" => "prod_Fr9YJzKxHAdARJ",
              "tiers" => nil,
              "tiers_mode" => nil,
              "transform_usage" => nil,
              "trial_period_days" => nil,
              "usage_type" => "licensed"
            },
            "quantity" => 1,
            "subscription" => "sub_Fr9gtlEuoJ9vtL",
            "tax_rates" => [
            ]
          }
        ],
        "has_more" => false,
        "total_count" => 1,
        "url" => "/v1/subscription_items?subscription=sub_Fr9gtlEuoJ9vtL"
      },
      "latest_invoice" => "in_1FLRMsLeD4GoxaIEu9jnciXP",
      "livemode" => false,
      "metadata" => %{
      },
      "pending_setup_intent" => nil,
      "plan" => %{
        "id" => "plan_Fr9b1qI2jX1UkO",
        "object" => "plan",
        "active" => true,
        "aggregate_usage" => nil,
        "amount" => 10_000,
        "amount_decimal" => "10000",
        "billing_scheme" => "per_unit",
        "created" => 1_569_144_255,
        "currency" => "eur",
        "interval" => "month",
        "interval_count" => 1,
        "livemode" => false,
        "metadata" => %{
        },
        "nickname" => "SaaS Platform USD",
        "product" => "prod_Fr9YJzKxHAdARJ",
        "tiers" => nil,
        "tiers_mode" => nil,
        "transform_usage" => nil,
        "trial_period_days" => nil,
        "usage_type" => "licensed"
      },
      "quantity" => 2,
      "schedule" => nil,
      "start" => 1_569_144_514,
      "start_date" => 1_569_144_514,
      "status" => "active",
      "tax_percent" => nil,
      "trial_end" => nil,
      "trial_start" => nil
    }

    CmcPayment.HTTPoisonMock
    |> expect(:post, fn _path, _data, _header ->
      {
        :ok,
        %HTTPoison.Response{body: Poison.encode!(response)}
      }
    end)

    assert Payment.Stripe.update_subscription("cus_FrJAjCNtzbcjgo", %{
      quantty: 2
    }) == 
      {:ok, response}
  end

  test "list_customer_invoices/1 return list of invoices for a customer" do
    message = File.read!("test/support/messages/invoices.json")

    CmcPayment.HTTPoisonMock
    |> expect(:get, fn "https://api.stripe.com/v1/invoices?customer=cus_FrJAjCNtzbcjgo", _header ->
      {
        :ok,
        %HTTPoison.Response{body: message}
      }
    end)

    assert Payment.Stripe.list_customer_invoices("cus_FrJAjCNtzbcjgo") == 
      {:ok, Poison.decode!(message)}
  end
end
