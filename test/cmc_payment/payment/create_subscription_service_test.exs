defmodule CmcPayment.Payment.CreateSubscriptionServiceTest do
  use CmcPayment.DataCase
  alias CmcPayment.Payment.Subscription
  alias CmcPayment.Payment.CreateSubscriptionService

  import Mox

  test "create_subscription/1 with valid data creates a subscription" do
    mock_stripe()
    valid_attrs = %{
      stripe_session_id: "some stripe_id",
      organization_id: 1,
      email: "test@example.com",
      quantity: 1
    }
    assert {:ok, %Subscription{} = subscription}
      = CreateSubscriptionService.create_subscription(valid_attrs)
    assert subscription.stripe_session_id == "some stripe_id"
    assert subscription.organization_id == 1
    assert subscription.email == "test@example.com"
    assert subscription.quantity == 1
  end

  test "create_subscription/1 with invalid does not creates a subscription" do
    invalid_attrs = %{}
    assert {:error, %Ecto.Changeset{} = changeset}
      = CreateSubscriptionService.create_subscription(invalid_attrs)
    assert changeset.valid? == false
  end

  test "create_subscription/1 when session does not exists return an error" do
    CmcPayment.Payment.StripeMock
    |> expect(:get_checkout_session, fn "some stripe_id" ->
         {
           :error,
           %HTTPoison.Response{status_code: 404}
         }
       end)
    attrs = %{
      stripe_session_id: "some stripe_id",
      organization_id: 1,
      email: "test@example.com",
      quantity: 1
    }
    assert {:error, %Ecto.Changeset{} = changeset}
      = CreateSubscriptionService.create_subscription(attrs)
    assert changeset.valid? == false
    assert changeset.errors == [stripe_session_id: {"stripe session not found", []}]
  end

  def mock_stripe do
    CmcPayment.Payment.StripeMock
    |> expect(:get_checkout_session, fn "some stripe_id" ->
         {
           :ok,
           %{"setup_intent" => "seti_1FLZxlLeD4GoxaIEUSek4Ib9"}
         }
       end)
    |> expect(:get_setup_intent, fn "seti_1FLZxlLeD4GoxaIEUSek4Ib9" ->
         {
           :ok,
           %{"payment_method" => "pm_1FLa6VLeD4GoxaIEJ6cmtEiD"}
         }
       end) 
    |> expect(:create_customer, fn %{email: "test@example.com"} ->
         {
           :ok,
           %{"id" => "cus_FrJAjCNtzbcjgo"}
         }
       end) 
    |> expect(:update_customer, fn(
         "cus_FrJAjCNtzbcjgo",
         %{default_payment_method: "pm_1FLa6VLeD4GoxaIEJ6cmtEiD"}
       ) ->
         {
           :ok,
           %{"id" => "cus_FrJAjCNtzbcjgo"}
         }
       end) 
    |> expect(:attach_payment_to_customer, fn _attrs ->
         {
           :ok,
           %{}
         }
       end) 
    |> expect(:create_subscription, fn %{
         customer: "cus_FrJAjCNtzbcjgo",
         plan: "plan_Fr9b1qI2jX1UkO",
         quantity: 1
       } ->
      {
        :ok,
        %{"id" => "sub_Fr9gtlEuoJ9vtL"}
      }
    end) 
  end
end
