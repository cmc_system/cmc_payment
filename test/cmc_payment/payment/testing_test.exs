defmodule CmcPayment.TestingTest do
  use CmcPayment.DataCase

  alias CmcPayment.Testing
  alias CmcPayment.Repo
  alias CmcPayment.Payment.Subscription

  @valid_subscription_attrs %CmcPayment.Payment.Subscription{
    stripe_session_id: "some stripe_id",
    stripe_subscription_id: "sub_FwZ0oDDH6KjJOZ",
    organization_id: 1,
    email: "test@example.com",
    quantity: 1
  }
  @valid_subscription_attrs2 %CmcPayment.Payment.Subscription{
    stripe_session_id: "some stripe_id",
    stripe_subscription_id: "sub_FwZ0oDDH6KjJOZ2",
    organization_id: 2,
    email: "test@example.com",
    quantity: 1
  }

  test "create!/0 creates test data" do
    {:ok, _} = Repo.insert(@valid_subscription_attrs)
    {:ok, _} = Repo.insert(@valid_subscription_attrs2)

    Testing.create!

    assert 1 = Enum.count(Repo.all(Subscription))
  end

  test "delete_all/0 delete all testing data" do
    {:ok, _} = Repo.insert(@valid_subscription_attrs)

    Testing.delete_all!

    assert [] = Repo.all(Subscription)
  end
end
