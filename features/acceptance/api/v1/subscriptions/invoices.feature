Feature: Customer invoices

Scenario: List
  Given seed data is loaded
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a GET request to "/api/v1/subscriptions/1/invoices"
  Then the response status should be "200"
  And the JSON response should be:
    """
    {
      "data": [{
        "id": "in_1FbueoLeD4GoxaIE6rVfYns7",
        "type": "invoices",
        "attributes": {
          "amountRemaining": 0,
          "amountPaid": 58668,
          "amountDue": 58668,
          "createdAt": "2019-11-06T19:59:10Z",
          "pdf": "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
        },
        "links": {
          "self": "http://localhost/api/v1/invoices/in_1FbueoLeD4GoxaIE6rVfYns7"
        },
        "relationships": {}
      },{
        "attributes": {
          "amountDue": 20000,
          "amountPaid": 20000,
          "amountRemaining": 0,
          "createdAt": "2019-10-06T19:59:04Z",
          "pdf": "https://pay.stripe.com/invoice/invst_7sNZM2hdIJubqnCNFhmJ4F5WeF/pdf"
        },
        "id": "in_1FQfsiLeD4GoxaIEtKwWfNRT",
        "links": {
          "self": "http://localhost/api/v1/invoices/in_1FQfsiLeD4GoxaIEtKwWfNRT"
        },
        "relationships": {},
        "type": "invoices"
      }],
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/invoices"                                                                                    
      }
    }
    """

Scenario: List invoices by organization
  Given seed data is loaded
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a GET request to "/api/v1/organizations/1/invoices"
  Then the response status should be "200"
  And the JSON response should be:
    """
    {
      "data": [{
        "id": "in_1FbueoLeD4GoxaIE6rVfYns7",
        "type": "invoices",
        "attributes": {
          "amountRemaining": 0,
          "amountPaid": 58668,
          "amountDue": 58668,
          "createdAt": "2019-11-06T19:59:10Z",
          "pdf": "https://pay.stripe.com/invoice/invst_9KtFtihugeF8KkYEfFEJltHcg7/pdf"
        },
        "links": {
          "self": "http://localhost/api/v1/invoices/in_1FbueoLeD4GoxaIE6rVfYns7"
        },
        "relationships": {}
      },{
        "attributes": {
          "amountDue": 20000,
          "amountPaid": 20000,
          "amountRemaining": 0,
          "createdAt": "2019-10-06T19:59:04Z",
          "pdf": "https://pay.stripe.com/invoice/invst_7sNZM2hdIJubqnCNFhmJ4F5WeF/pdf"
        },
        "id": "in_1FQfsiLeD4GoxaIEtKwWfNRT",
        "links": {
          "self": "http://localhost/api/v1/invoices/in_1FQfsiLeD4GoxaIEtKwWfNRT"
        },
        "relationships": {},
        "type": "invoices"
      }],
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/invoices"                                                                                    
      }
    }
    """
