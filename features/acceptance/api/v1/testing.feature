Feature: Manage test data.

Scenario: Create test data
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/testing"
  Then the response status should be "201"

Scenario: Delete test data
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a DELETE request to "/api/v1/testing"
  Then the response status should be "200"
