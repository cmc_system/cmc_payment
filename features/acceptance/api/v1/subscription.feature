Feature: Subscription

Scenario: get a subscription
  Given seed data is loaded
  And The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a GET request to "/api/v1/subscriptions/1"
  Then the response status should be "200"
  And the JSON response should be:
    """
    {
      "data": {
        "attributes": {
          "stripeSessionId": "cs_test_xpK6Pq8jMXbcxlV41ESuACxwqEosAGNM52zTSiqsp7PO1TnuJ23vCUPM",
          "organizationId": 1, 
          "quantity": 1,
          "ibanLast4": "4242", 
          "email": "test@example.com",
          "premia": 0
        },
        "id": "1",
        "links": {
          "self": "http://localhost/api/v1/subscriptions/1" 
        },
        "relationships": {},
        "type": "subscriptions"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/subscriptions/1"
      }
    }
    """

Scenario: get a subscription by organization
  Given seed data is loaded
  And The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a GET request to "/api/v1/organizations/1/subscription"
  Then the response status should be "200"
  And the JSON response should be:
    """
    {
      "data": {
        "attributes": {
          "stripeSessionId": "cs_test_xpK6Pq8jMXbcxlV41ESuACxwqEosAGNM52zTSiqsp7PO1TnuJ23vCUPM",
          "organizationId": 1, 
          "quantity": 1,
          "ibanLast4": "4242", 
          "email": "test@example.com",
          "premia": 0
        },
        "id": "1",
        "links": {
          "self": "http://localhost/api/v1/subscriptions/1" 
        },
        "relationships": {},
        "type": "subscriptions"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/subscriptions/1"
      }
    }
    """

Scenario: create a subscription
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/subscriptions" with the following:
    """
    {
      "data": {
        "attributes": {
          "stripe_session_id": "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50", 
          "organization_id": "1",
          "email": "test@example.com",
          "quantity": 1
        }
      }
    }
    """
  Then the JSON response should be:
    """
    {
      "data": {
        "attributes": {
          "stripeSessionId": "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50",
          "organizationId": 1, 
          "email": "test@example.com",
          "quantity": 1
        },
        "id": "1",
        "links": {
          "self": "http://localhost/api/v1/subscriptions/1" 
        },
        "relationships": {},
        "type": "subscriptions"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/subscriptions/1"
      }
    }
    """
  And the response status should be "201"

Scenario: create a subscription with invalid data
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/subscriptions" with the following:
    """
    {
      "data": {
        "attributes": {
        }
      }
    }
    """
  Then the JSON response should be:
    """
    {
      "jsonapi": { "version": "1.0"}, 
      "errors":[{
        "title": "can't be blank",
        "source": { "pointer": "/data/attributes/email" },
        "detail": "Email can't be blank"
      }, {
        "title": "can't be blank",
        "source": { "pointer": "/data/relationships/organization"},
        "detail": "Organization can't be blank"
      }, {
        "title": "can't be blank",
        "source": { "pointer": "/data/relationships/stripe-session"},
        "detail": "Stripe session can't be blank"
      }]
    }
    """
  And the response status should be "422"
Scenario: update a subscription
  Given seed data is loaded
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a PATH request to "/api/v1/subscriptions/1" with the following:
    """
    {
      "data": {
        "attributes": {
          "stripe_session_id": "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50", 
          "quantity": 2
        }
      }
    }
    """
  Then the response status should be "200"
  And the JSON response should be:
    """
    {
      "data": {
        "attributes": {
          "stripeSessionId": "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50",
          "organizationId": 1, 
          "email": "test@example.com",
          "quantity": 2
        },
        "id": "1",
        "links": {
          "self": "http://localhost/api/v1/subscriptions/1" 
        },
        "relationships": {},
        "type": "subscriptions"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/subscriptions/1"
      }
    }
    """
