Feature: Stripe checkout session

Scenario: create a stripe checkout session
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/stripe_session" with the following:
    """
    {
      "data": {
        "attributes": {
          "success_url": "https://www.example.com/success_url", 
          "cancel_url": "https://www.example.com/cancel_url"
        }
      }
    }
    """
  Then the JSON response should be:
    """
    {
      "data": {
        "attributes": {},
        "id": "cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50",
        "links": {
          "self": "http://localhost/api/v1/stripe_session/cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50"                                                                                  
        },
        "relationships": {},
        "type": "stripe_session"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/stripe_session/cs_test_8fh4AlX39Ncd3y8Rz2ysYSFOhPm8a4xkP4F2TnVOGnAMb61zyuCxXr50"                                                                                    
      }
    }
    """
  And the response status should be "201"

Scenario: Try to create a stripe checkout session with invalid data
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/stripe_session" with the following:
    """
    {
      "data": {
        "attributes": {
        }
      }
    }
    """
  Then the JSON response should be:
    """
    {
      "jsonapi": {"version": "1.0"},
      "errors": [{
        "title": "can't be blank",
        "source": {"pointer": "/data/attributes/cancel-url"},
        "detail": "Cancel url can't be blank"
      }, {
        "title": "can't be blank",
        "source": {"pointer": "/data/attributes/success-url"},
        "detail": "Success url can't be blank"
      }]
    }
    """
  And the response status should be "422"
