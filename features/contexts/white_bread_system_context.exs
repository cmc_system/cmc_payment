defmodule WhiteBreadSystemContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Hound.Helpers.Element

  scenario_starting_state fn state ->
    Application.ensure_all_started(:hound)
    Hound.start_session(driver: %{chromeOptions: %{"args" => [
      "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
      "--disable-dev-shm-usage"
    ]}})
    state
  end

  scenario_finalize fn state ->
    Hound.end_session
  end

  def wait_for(timeout, func) do
    :timer.sleep(100)
    case func.() || timeout <= 0 do
      true -> true
      false -> wait_for(timeout - 100, func)
    end
  end

  given_ ~r/^The following headers:$/, fn state, %{table_data: table_data} ->
    {:ok, state |> put_in([:headers], table_data |> Enum.map(fn %{key: key, value: value} -> {key, value} end))}
  end

  when_ ~r/^I send a POST request to "(?<argument_one>[^"]+)" with the following:$/,                                   
       fn state, %{argument_one: argument_one, doc_string: doc_string} ->
    {:ok, response} = HTTPoison.post(
      "http://localhost:4001/#{argument_one}",
      doc_string,
      state.headers
    )
    {:ok, state |> put_in([:response], response)}
  end

  when_ ~r/^I send a GET request to "(?<argument_one>[^"]+)"$/, 
       fn state, %{argument_one: argument_one, doc_string: doc_string} ->
    {:ok, response} = HTTPoison.get(
      "http://localhost:4001/#{argument_one}",
      state.headers
    )
    {:ok, state |> put_in([:response], response)}
  end

  when_ ~r/^I send a GET request to stripe \/subscriptions\/:id$/, 
       fn state, _ ->
    subscription = CmcPayment.Repo.one(CmcPayment.Payment.Subscription)
    {:ok, response} = HTTPoison.get(
      "https://api.stripe.com/v1/subscriptions/#{subscription.stripe_subscription_id}",
      state.headers
    )
    {:ok, state |> put_in([:response], response)}
  end

  when_ ~r/^I open "\/stripe_checkout"$/, fn state, _ ->
    stripe_session_id = Poison.decode!(state[:response].body)["data"]["id"]
    navigate_to("/stripe_checkout?stripe_session_id=#{stripe_session_id}")
    {:ok, state}
  end

  when_ ~r/^I fill "(?<name>[^"]+)" with "(?<value>[^"]+)"$/,
        fn state, %{name: name, value: value} ->
    element = find_element(:name, name)
    fill_field(element, value)
    {:ok, state}
  end

  when_ ~r/^I select "(?<value>[^"]+)" from "(?<element>[^"]+)"$/,
        fn state, %{element: element, value: value} ->
    find_element(:css, "##{element} option[value='#{value}']") |> Element.click
    {:ok, state}
  end

  when_ ~r/^I submit form with input "(?<name>[^"]+)"$/,
        fn state, %{name: name} ->
    element = find_element(:name, name)
    submit_element(element)
    {:ok, state}
  end

  when_ ~r/^I create a subscription with the session id$/, fn state ->
    query = URI.parse(current_url()).query
    session_id = URI.decode_query(query)["session_id"]
    {:ok, response} =  HTTPoison.post(
      "http://localhost:4001/api/v1/subscriptions",
      Poison.encode!(%{
        "data": %{
          "attributes": %{
            "stripe_session_id": session_id, 
            "organization_id": "1",
            "email": "test@example.com",
            "quantity": 2
          }
        }
      }),
      state.headers,
      [recv_timeout: 7000]
    )
    state =
      state
      |> put_in([:result_session_id], session_id)
      |> put_in([:response], response)
    {:ok, state}
  end

  then_ ~r/^the JSON response should be:$/,
       fn state, %{doc_string: doc_string} ->
    expectation =
      Poison.decode!(doc_string)
      |> put_in(["data", "attributes", "stripeSessionId"], state[:result_session_id])
    assert Poison.decode!(state[:response].body) == expectation
    {:ok, state}
  end

  then_ ~r/^the response body should contain '(?<to_include>[^']+)'$/,
       fn state, %{to_include: to_include} ->
    assert String.contains?(state[:response].body, to_include)
    {:ok, state}
  end

  when_ ~r/^I wait for the page "(?<page>[^"]+)"$/,
  fn state, %{page: page} ->
    wait_for(60_000, fn -> Regex.match?(~r/#{page}/, current_url()) end)
    take_screenshot("screenshots/#{page}.png")
    {:ok, state}
  end

  then_ ~r/^the response status should be "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    assert state[:response].status_code == String.to_integer(argument_one)
    {:ok, state}
  end
end
