defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Hound.Helpers.Element
  alias CmcPayment.Repo
  alias CmcPayment.Payment.Subscription

  scenario_starting_state fn state ->
    Repo.delete_all(Subscription)
    Repo.query!("ALTER SEQUENCE subscriptions_id_seq RESTART")
    Application.ensure_all_started(:hound)
    Hound.start_session(driver: %{chromeOptions: %{"args" => [
      "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
      "--disable-dev-shm-usage"
    ]}})
    state
  end

  scenario_finalize fn state ->
    Hound.end_session
  end

  def wait_for(timeout, func) do
    :timer.sleep(100)
    case func.() || timeout <= 0 do
      true -> true
      false -> wait_for(timeout - 100, func)
    end
  end

  given_ ~r/^The following headers:$/, fn state, %{table_data: table_data} ->
    {:ok, state |> put_in([:headers], table_data |> Enum.map(fn %{key: key, value: value} -> {key, value} end))}
  end

  given_ ~r/^seed data is loaded$/, fn state, _ ->
    Code.eval_file "priv/repo/seeds.exs"
    {:ok, state}
  end

  when_ ~r/^I send a POST request to "(?<argument_one>[^"]+)" with the following:$/,                                   
       fn state, %{argument_one: argument_one, doc_string: doc_string} ->
    HTTPoison.post(
      "http://localhost:4001/#{argument_one}",
      doc_string,
      state.headers
    )
  end

  when_ ~r/^I send a PATH request to "(?<argument_one>[^"]+)" with the following:$/,                                   
       fn state, %{argument_one: argument_one, doc_string: doc_string} ->
    HTTPoison.patch(
      "http://localhost:4001/#{argument_one}",
      doc_string,
      state.headers
    )
  end

  when_ ~r/^I send a POST request to "(?<argument_one>[^"]+)"$/,                                   
       fn state, %{argument_one: argument_one} ->
    HTTPoison.post(
      "http://localhost:4001/#{argument_one}",
      "",
      state.headers
    )
  end

  when_ ~r/^I send a GET request to "(?<argument_one>[^"]+)"$/,                                   
       fn state, %{argument_one: argument_one} ->
    HTTPoison.get(
      "http://localhost:4001/#{argument_one}",
      state.headers
    )
  end

  when_ ~r/^I send a DELETE request to "(?<argument_one>[^"]+)"$/,                                   
       fn state, %{argument_one: argument_one} ->
    HTTPoison.delete(
      "http://localhost:4001/#{argument_one}",
      state.headers
    )
  end

  when_ ~r/^I open "(?<path>[^"]+)"$/, fn state, %{path: path} ->
    navigate_to(path)
    {:ok, state}
  end

  when_ ~r/^I wait for the page "(?<page>[^"]+)"$/,
  fn state, %{page: page} ->
    wait_for(60_000, fn -> Regex.match?(~r/#{page}/, current_url()) end)
    take_screenshot("screenshots/#{page}.png")
    {:ok, state}
  end

  then_ ~r/^the JSON response should be:$/,
       fn state, %{doc_string: doc_string} ->
    assert Poison.decode!(state.body) == Poison.decode!(doc_string)
    {:ok, state}
  end

  then_ ~r/^the response status should be "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    assert state.status_code == String.to_integer(argument_one)
    {:ok, state}
  end
end
