Feature: Subscription

Feature: Stripe checkout session

Scenario: stripe works
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a POST request to "/api/v1/stripe_session" with the following:
    """
    {
      "data": {
        "attributes": {
          "success_url": "https://example.com/success?session_id={CHECKOUT_SESSION_ID}", 
          "cancel_url": "https://example.com/cancel"
        }
      }
    }
    """
  When I open "/stripe_checkout"
  When I wait for the page "checkout.stripe.com"
  When I fill "email" with "test@example.com"
  When I fill "cardNumber" with "4242424242424242"
  When I fill "cardExpiry" with "12/40"
  When I fill "cardCvc" with "1234"
  When I fill "billingName" with "Lara Croft"
  When I select "DE" from "billingCountry"
  When I submit form with input "billingName"
  When I wait for the page "example.com"
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I create a subscription with the session id
  Then the JSON response should be:
    """
    {
      "data": {
        "attributes": {
          "stripeSessionId": "<stripe_session_id>",
          "organizationId": 1, 
          "email": "test@example.com",
          "quantity": 2
        },
        "id": "1",
        "links": {
          "self": "http://localhost/api/v1/subscriptions/1" 
        },
        "relationships": {},
        "type": "subscriptions"
      },
      "included": [],
      "links": {
        "self": "http://localhost/api/v1/subscriptions/1"
      }
    }
    """
  And the response status should be "201"
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic YWRtaW46dGVzdHRlc3Q=     |
    | Content-Type  | application/json               |
  When I send a GET request to "/api/v1/organizations/1/subscription"
  Then the response status should be "200"
  And the response body should contain '"quantity":2'
  When I send a GET request to "/api/v1/organizations/1/invoices"
  Then the response status should be "200"
  Given The following headers:
    | key           | value                          |
    | Authorization | Basic c2tfdGVzdF92OWVNNnZCM1NkT1dmeTZKQ1VqQjU4RWgwMG5oS09jaVNmOg== |
  When I send a GET request to stripe /subscriptions/:id
  Then the response status should be "200"
  And the response body should contain '"quantity": 2'
