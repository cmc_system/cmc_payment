defmodule WhiteBreadConfig do
  use WhiteBread.SuiteConfiguration

  suite name:          "Acceptance",
        context:       WhiteBreadContext,
        feature_paths: ["features/acceptance/"]

  suite name:          "System",
        context:       WhiteBreadSystemContext,
        feature_paths: ["features/system/"]
end
