# CmcPayment

docs: [`https://cmc_system.gitlab.io/cmc_payment`](https://cmc_system.gitlab.io/cmc_payment)

## Develop setup
Execute on a terminal:
  * docker-compose run app mix deps.get
  * docker-compose run app mix ecto.setup
  * docker-compose up

You can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Test
* Unit tests: docker-compose run app mix test
* Integration/Acceptance tests: docker-compose run app mix cucumber
* Lint: docker-compose run app mix credo

## New release
Every commit to the master branch will automaticly deployed
by the GitlabCi.

## Production setup
* Create a kubernetes cluster
* Encode base64 the private key of stripe
  `echo -n 'pk_live_whatever' | base64`
* Create a secret
  *
  ```
  apiVersion: v1
  kind: Secret
  metadata:
    name: cmc-payment-prod-chart
    data:
      stripe_private_secret: bXktYXBw
  ```
  * kubectl apply -f secret.yml
* helm install cmc-payment-prod-chart ./chart

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
