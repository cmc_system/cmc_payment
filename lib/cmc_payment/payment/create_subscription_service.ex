defmodule CmcPayment.Payment.CreateSubscriptionService do
  alias CmcPayment.Payment.Subscription
  alias CmcPayment.Repo

  @moduledoc """
  See CmcPayment.Payment.create_subscription/1
  """

  @stripe Application.get_env(:cmc_payment, :payment_stripe)

  @doc false
  def create_subscription(attrs \\ %{}) do
    changeset = Subscription.create_changeset(attrs)
    with {:ok, subscription} <- Ecto.Changeset.apply_action(changeset, :insert),
         {:ok, session} <- get_stripe_checkout_session(subscription.stripe_session_id),
         {:ok, setup_intent} <- @stripe.get_setup_intent(session["setup_intent"]),
         {:ok, customer} <- @stripe.create_customer(%{email: subscription.email}),
         {:ok, _} <-
           @stripe.attach_payment_to_customer(%{
             customer: customer["id"],
             payment_method: setup_intent["payment_method"]
           }),
         {:ok, _} <-
           @stripe.update_customer(
             customer["id"],
             %{default_payment_method: setup_intent["payment_method"]}
           ),
         {:ok, stripe_subscription} <-
           @stripe.create_subscription(%{
             customer: customer["id"],
             plan: stripe_plan_id(),
             quantity: subscription.quantity
           })
    do
      {:ok, subscription} =
        insert_subscription(subscription, %{
          stripe_customer_id: customer["id"],
          stripe_subscription_id: stripe_subscription["id"],
        })
      {:ok, subscription}
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        {:error, changeset}
      {:error, :stripe_session_not_found} ->
        changeset = changeset
        |> Ecto.Changeset.add_error(:stripe_session_id, "stripe session not found")
        |> Map.put(:action, :insert)
        {:error, changeset}
    end
  end

  defp get_stripe_checkout_session(stripe_session_id) do
    case @stripe.get_checkout_session(stripe_session_id) do
      {:ok, session} -> {:ok, session}
      {:error, %HTTPoison.Response{status_code: 404}} ->
        {:error, :stripe_session_not_found}
    end
  end

  defp insert_subscription(subscription, %{
    stripe_subscription_id: stripe_subscription_id,
    stripe_customer_id: stripe_customer_id
  }) do
    subscription
    |> Map.put(:stripe_customer_id, stripe_customer_id)
    |> Map.put(:stripe_subscription_id, stripe_subscription_id)
    |> Repo.insert()
  end
  
  defp stripe_plan_id do
    Application.get_env(:cmc_payment, :stripe_plan_id)
  end
end
