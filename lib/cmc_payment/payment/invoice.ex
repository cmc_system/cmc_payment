defmodule CmcPayment.Payment.Invoice do
  @moduledoc """
  Invoice coming from stripe. It contain only a subset
  of the attributes coming from stripe.
  """

  use Ecto.Schema

  embedded_schema do
    field :created_at
    field :amount_due
    field :amount_paid
    field :amount_remaining
    field :pdf
  end
end
