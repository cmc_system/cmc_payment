defmodule CmcPayment.Payment.Subscription do
  @moduledoc """
  The subscription database table is used to store all
  the necesarry informations related to a subscription.
  For example, stripe store a customer, a payment method etc.
  all this informations could be stored in diferent tables but we
  descided to stroe all in the same one.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias CmcPayment.Repo

  schema "subscriptions" do
    field :stripe_session_id, :string
    field :organization_id, :integer
    field :email, :string
    field :stripe_customer_id, :string
    field :stripe_subscription_id, :string
    field :quantity, :integer, default: 0
    field :iban_last4, :string, virtual: true
    field :premia, :integer, virtual: true

    timestamps()
  end

  @doc false
  def create_changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:stripe_session_id])
    |> cast(attrs, [:organization_id])
    |> cast(attrs, [:email])
    |> cast(attrs, [:quantity])
    |> validate_required([:stripe_session_id])
    |> validate_required([:organization_id])
    |> validate_required([:email])
    |> validate_required([:quantity])
    |> validate_number(:quantity, greater_than_or_equal_to: 0)
    |> unsafe_validate_unique([:organization_id], Repo)
    |> unique_constraint(:organization_id)
  end

  @doc false
  def update_changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [:stripe_session_id])
    |> cast(attrs, [:email])
    |> cast(attrs, [:quantity])
    |> validate_required([:stripe_session_id])
    |> validate_required([:email])
    |> validate_required([:quantity])
    |> validate_number(:quantity, greater_than_or_equal_to: 0)
  end
end
