defmodule CmcPayment.Payment.Charge do
  @moduledoc """
  Some services, like PayPal, have a susbcription
  system that is not compatible with our suscription
  system. Bucause of this, the customer can charge a
  amount of money on his account.
  """

  use Ecto.Schema
  import Ecto.Changeset


  schema "charges" do
    field :amount, :decimal

    timestamps()
  end

  @doc false
  def changeset(charge, attrs) do
    charge
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
  end
end
