require Logger

defmodule CmcPayment.Payment.Premia do
  @moduledoc """
  The premia is the amount that a customer has to pay per mount
  in euros.

  max(5 * quantity - 25, 0)
  """

  @doc """
  ## Examples

      iex> calcule(%{
        amount: 1,
      })
      0

  """
  @callback calcule(struct()) :: integer()
  def calcule(%{quantity: quantity}) do
    [0, quantity * 5 - 25] |> Enum.max
  end
end
