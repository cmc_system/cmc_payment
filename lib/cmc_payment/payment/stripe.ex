require Logger

defmodule CmcPayment.Payment.Stripe do
  @moduledoc """
  This module should be used to interact with the stripe payment api.
  The stripe payment api can be used to charge money to the customers.
  Eg charge a amount per user, every month.
  """

  @httpoison Application.get_env(:cmc_payment, :httpoison)

  @doc """
  Creates a stripe session. The stripe session
  can be used to open a stripe webpage with
  a form where the payment information can be added
  by the customer.

  ## Examples

      iex> create_stripe_session(%{
        cancel_url: "www.example.com/cancel_url",
        success_url: "www.example.com/success_url"
      })
      %{id: "testid"}

  """
  @callback create_checkout_session(struct()) :: struct()
  def create_checkout_session(%{cancel_url: cancel_url, success_url: success_url}) do
    data = %{
      "cancel_url" => cancel_url,
      "success_url" => success_url,
      "payment_method_types[]" => "card",
      "mode" => "setup"
    }
    post_request("v1/checkout/sessions", data)
  end

  @doc """
  Get a checkout stripe checkout session. A stripe checkout
  session can be used to open a stripe dialog where
  the cutomer can add his payment informations (like iban).
  
  See also: https://stripe.com/docs/api/checkout/sessions

  ## Examples

      iex> get_checkout_session("valid_stripe_checkout_session_id")
      {:ok, %{id: "stripe_checkout_session_id", ...}}

      iex> get_checkout_session("invalid_stripe_checkout_session_id")
      {:error, %HTTPoison.Response{}} 

  """
  @callback get_checkout_session(struct()) :: struct()
  def get_checkout_session(session_id) do
    get_request("v1/checkout/sessions/#{session_id}")
  end

  @doc """
  Get a stripe setup intent. A stripe setup intent can
  be used to get the payment method_id. The payment method
  id can be used to attach the payment informations to a customer. 

  See also: https://stripe.com/docs/api/payment_intents

  ## Examples

      iex> get_setup_intent("valid_stripe_setup_intent_id")
      {:ok, %{id: "valid_stripe_setup_intent_id", ...}}

      iex> get_checkout_session("invalid_stripe_checkout_session_id")
      {:error, %HTTPoison.Response{}} 

  """
  @callback get_setup_intent(struct()) :: struct()
  def get_setup_intent(setup_intent) do
    get_request("v1/setup_intents/#{setup_intent}")
  end

  @doc """
  Get a stripe subscription. It returns a subscription with
  its customer and his default payment method.

  See also:
  * https://stripe.com/docs/api/subscriptions/retrieve
  * https://stripe.com/docs/api/expanding_objects

  ## Examples

      iex> get_subscription(email: "test@example.com")
      {:ok, %{id: "sub_FwZ0oDDH6KjJOZ", ...}}

      iex> create_customer(email: nil)
      {:error, %HTTPoison.Response{}} 

  """
  @callback get_subscription(string()) :: struct()
  def get_subscription(stripe_subscription_id) do
    get_request("/v1/subscriptions/#{stripe_subscription_id}?expand[]=customer.invoice_settings.default_payment_method")
  end

  @doc """
  Create a stripe customer. A stripe customer can
  have subscriptions to stripe payment plans.

  See also: https://stripe.com/docs/api/customers

  ## Examples

      iex> create_customer(email: "test@example.com")
      {:ok, %{id: "customer_id", ...}}

      iex> create_customer(email: nil)
      {:error, %HTTPoison.Response{}} 

  """
  @callback create_customer(struct()) :: struct()
  def create_customer(attrs \\ %{}) do
    post_request("v1/customers", attrs)
  end

  @doc """
  Update a stripe customer. It can be used to set
  the default_payment_method. The default payment
  method can not be set on customer creation
  because it has first to be attached to the
  customer.

  See also:
  * https://stripe.com/docs/api/customers
  * https://stripe.com/docs/payments/checkout/subscriptions/updating#set

  ## Examples

      iex> create_customer(email: "test@example.com")
      {:ok, %{id: "customer_id", ...}}

      iex> create_customer(email: nil)
      {:error, %HTTPoison.Response{}} 

  """
  @callback update_customer(string(), struct()) :: struct()
  def update_customer(id, attrs \\ %{}) do
    params = %{
      "invoice_settings[default_payment_method]": attrs[:default_payment_method] 
    }
    params = params
    |> Enum.filter(fn {_, value} -> !is_nil(value) end)
    |> Enum.into(%{})

    post_request("v1/customers/#{id}", params)
  end

  @doc """
  Attach a stripe payment method to a customer.
  It return the payment method, passed as id.

  See also: https://stripe.com/docs/api/payment_methods/attach

  ## Examples

      iex> attach_payment_to_customer%{
        customer: "customer_id",
        payment_method: "payment_method_id"
      }
      {:ok, %{id: "payment_method_id", ...}}

      iex> attach_payment_to_customer%{
        customer: "customer_id",
        payment_method: "invalid_payment_method_id"
      }
      {:error, %HTTPoison.Response{}} 

  """
  @callback attach_payment_to_customer(struct()) :: struct()
  def attach_payment_to_customer(%{customer: customer, payment_method: payment_method}) do
    post_request("v1/payment_methods/#{payment_method}/attach", %{customer: customer})
  end

  @doc """
  Create a stripe subscription.

  See also: https://stripe.com/docs/api/subscriptions

  ## Examples

      iex> create_subscription%{
        customer: "customer_id",
        plan: "plan_id"
      }
      {:ok, %{id: "subscription_id", ...}}

      iex> create_subscription%{
        customer: "invalid_customer_id",
        plan: "plan_id",
        quantity: 1
      }
      {:error, %HTTPoison.Response{}} 

  """
  @callback create_subscription(struct()) :: struct()
  def create_subscription(%{customer: customer, plan: plan} = attrs) do
    params = %{
      customer: customer,
      "items[0][plan]": plan,
      "items[0][quantity]": attrs[:quantity],
      coupon: "free-amount"
    }
    params = params
    |> Enum.filter(fn {_, value} -> !is_nil(value) end)
    |> Enum.into(%{})
    post_request("v1/subscriptions", params)
  end

  @doc """
  Update a stripe subscription.

  See also: https://stripe.com/docs/api/subscriptions

  ## Examples

      iex> update_subscription%{
        quantty: 2,
      }
      {:ok, %{id: "subscription_id", ...}}

      iex> update_subscription%{
        quantity: -1
      }
      {:error, %HTTPoison.Response{}} 

  """
  @callback update_subscription(string(), struct()) :: struct()
  def update_subscription(subscription_id, attrs) do
    params = %{
      quantity: attrs[:quantity]
    }
    params = params
    |> Enum.filter(fn {_, value} -> !is_nil(value) end)
    |> Enum.into(%{})
    post_request("v1/subscriptions/#{subscription_id}", params)
  end

  @doc """
  Return invoices for a customer

  See also: https://stripe.com/docs/api/invoices/list

  ## Examples

      iex> customer_invoices("cus_FrJAjCNtzbcjgo")
      {:ok, %{"object": "list", "data": [{}]}

      iex> customer_invoices("not_exist_customer_id")
      {:error, %HTTPoison.Response{}} 

  """
  @callback list_customer_invoices(string()) :: struct()
  def list_customer_invoices(customer) do
    get_request("v1/invoices?customer=#{customer}")
  end

  defp post_request(endpoint, attrs) do
    url = "#{stripe_url()}/#{endpoint}"
    Logger.info("stripe api post request body: #{Poison.encode!(attrs)}")
    Logger.info("stripe api post request url: #{url}")
    result = @httpoison.post(url, URI.encode_query(attrs), headers())
    {_, response} = result
    Logger.info("stripe api post response body: #{response.body}")
    Logger.info("stripe api post response status: #{response.status_code}")

    case result do
      {:ok, response} -> {:ok, Poison.decode!(response.body)}
      {:error, response} -> {:error, response}
    end
  end

  defp get_request(endpoint) do
    url = [stripe_url(), endpoint] |> Enum.join("/")
    Logger.info("stripe api get request url: #{url}")
    result = @httpoison.get(url, headers())
    {_, response} = result
    Logger.info("stripe api get response body: #{response.body}")
    Logger.info("stripe api get response status: #{response.status_code}")

    case result do
      {:ok, %HTTPoison.Response{status_code: 404} = response} -> {:error, response}
      {:ok, response} -> {:ok, Poison.decode!(response.body)}
      {:error, response} -> {:error, response}
    end
  end

  defp headers do
    basic_auth = Base.encode64("#{username()}:#{password()}")
    %{
      "Content-Type" => "application/x-www-form-urlencoded",
      "Authorization" => "Basic #{basic_auth}"
     }
  end
  
  defp stripe_url do
    Application.get_env(:cmc_payment, :stripe_url)
  end

  defp username do
    Application.get_env(:cmc_payment, :stripe_username)
  end

  defp password do
    Application.get_env(:cmc_payment, :stripe_password)
  end
end
