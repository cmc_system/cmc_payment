defmodule CmcPayment.Payment.UpdateSubscriptionService do
  alias CmcPayment.Payment.Subscription
  alias CmcPayment.Repo

  @moduledoc """
  See CmcPayment.Payment.update_subscription/2
  """

  @stripe Application.get_env(:cmc_payment, :payment_stripe)

  @doc false
  def update_subscription(subscription, attrs \\ %{}) do
    with changeset <- Subscription.update_changeset(subscription, attrs),
         {true, changeset} <- {changeset.valid?, changeset},
         subscription <- Ecto.Changeset.apply_changes(changeset),
         {:ok, session} <- @stripe.get_checkout_session(subscription.stripe_session_id),
         {:ok, setup_intent} <- @stripe.get_setup_intent(session["setup_intent"]),
         {:ok, _} <-
           @stripe.attach_payment_to_customer(%{
             customer: subscription.stripe_customer_id,
             payment_method: setup_intent["payment_method"]
           }),
         {:ok, _} <-
           @stripe.update_customer(
             subscription.stripe_customer_id,
             %{default_payment_method: setup_intent["payment_method"]}
           ),
         {:ok, _} <-
           @stripe.update_subscription(subscription.stripe_subscription_id, %{
             quantity: subscription.quantity
           })
    do
      {:ok, _} = Repo.update(changeset)
    else
      {:false, changeset} ->
        {:error, changeset}
    end
  end
end
