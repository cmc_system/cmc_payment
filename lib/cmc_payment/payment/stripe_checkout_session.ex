defmodule CmcPayment.Payment.StripeCheckoutSession do
  @moduledoc """
  It is used for valdiate the data, before
  sending it to strpe to create a stripe checkout
  session.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "subscriptions" do
    field :success_url, :string, virtual: true
    field :cancel_url, :string, virtual: true
  end

  @doc false
  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:success_url])
    |> cast(attrs, [:cancel_url])
    |> validate_required([:success_url])
    |> validate_required([:cancel_url])
  end
end
