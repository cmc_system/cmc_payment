defmodule CmcPayment.Payment do
  @moduledoc """
  The Payment context.
  """

  import Ecto.Query, warn: false
  alias CmcPayment.Repo
  alias CmcPayment.Payment.Subscription
  alias CmcPayment.Payment.StripeCheckoutSession

  @stripe_url Application.get_env(:cmc_payment, :stripe_url)
  @username Application.get_env(:cmc_payment, :stripe_username)
  @password Application.get_env(:cmc_payment, :stripe_password)
  @httpoison Application.get_env(:cmc_payment, :httpoison)
  @stripe Application.get_env(:cmc_payment, :payment_stripe)
  @premia Application.get_env(:cmc_payment, :premia)

  @doc """
  Creates a stripe session. The stripe session
  can be used to open a stripe webpage with
  a form where the payment information can be added
  by the customer.

  ## Examples

      iex> create_checkout_session(%{
        cancel_url: "www.example.com/cancel_url",
        success_url: "www.example.com/success_url"
      })
      {:ok, %{id: "testid"}}

      iex> create_checkout_session(%{})
      {:error, %Ecto.Changeset{}}

  """
  @callback create_checkout_session(struct()) :: struct()
  def create_checkout_session(attrs \\ %{}) do
    case StripeCheckoutSession.changeset(attrs) |> Ecto.Changeset.apply_action(:insert) do
      {:ok, checkout_session} ->
        {:ok, checkout_session} = @stripe.create_checkout_session(checkout_session)
        if checkout_session["error"] do
          {
            :error,
            %Ecto.Changeset{errors: [base: {checkout_session["error"]["message"], []}]}
          }
        else
          {:ok, checkout_session}
        end
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Returns the list of subscriptions.

  ## Examples

      iex> list_subscriptions()
      [%Subscription{}, ...]

  """
  @callback list_subscriptions() :: [struct]
  def list_subscriptions do
    Repo.all(Subscription)
  end

  @doc """
  Returns a subscription. The restult is a composition
  of the data in the database and data from the stripe
  api endpoint.

  ## Examples

      iex> get_subscription!(1)
      {:ok, %{id: 1, ...}}

      iex> get_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  @callback get_subscription!(string()) :: struct()
  def get_subscription!(id) do
    subscription = Repo.get!(CmcPayment.Payment.Subscription, id)
    {:ok, result} = @stripe.get_subscription(subscription.stripe_subscription_id)
    subscription
    |> Map.put(:iban_last4, result["customer"]["invoice_settings"]["default_payment_method"]["card"]["last4"])
    |> Map.put(:premia, @premia.calcule(%{
      quantity: subscription.quantity
    }))
  end

  @doc """
  Returns a subscription. The result is a composition
  of the data in the database and data from the stripe
  api endpoint.

  ## Examples

      iex> get_subscription_by_organization!(1)
      {:ok, %{id: 1, ...}}

      iex> get_subscription_by_organization!(456)
      ** (Ecto.NoResultsError)

  """
  @callback get_subscription_by_organization!(string()) :: struct()
  def get_subscription_by_organization!(organization_id) do
    subscription = Repo.get_by!(
      CmcPayment.Payment.Subscription, 
      organization_id: organization_id
    )
    {:ok, result} = @stripe.get_subscription(subscription.stripe_subscription_id)
    subscription
    |> Map.put(:iban_last4, result["customer"]["invoice_settings"]["default_payment_method"]["card"]["last4"])
    |> Map.put(:premia, @premia.calcule(%{
      quantity: subscription.quantity
    }))
  end

  @doc """
  Creates a subscription.

  ## Examples

      iex> create_subscription(%{field: value})
      {:ok, %Subscription{}}

      iex> create_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @callback create_subscription(struct()) :: struct()
  defdelegate create_subscription(attrs \\ %{}),
    to: CmcPayment.Payment.CreateSubscriptionService

  @doc """
  Updates a subscription.

  ## Examples

      iex> update_subscription(subscription, %{field: new_value})
      {:ok, %Subscription{}}

      iex> update_subscription(subscription, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @callback update_subscription(struct(), struct()) :: struct()
  defdelegate update_subscription(subscription, attrs),
    to: CmcPayment.Payment.UpdateSubscriptionService

  @doc """
  Deletes a Subscription.

  ## Examples

      iex> delete_subscription(subscription)
      {:ok, %Subscription{}}

      iex> delete_subscription(subscription)
      {:error, %Ecto.Changeset{}}

  """
  @callback delete_subscription(struct()) :: {:ok, struct} | {:error, struct}
  def delete_subscription(%Subscription{} = subscription) do
    Repo.delete(subscription)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subscription changes
  on creation.

  ## Examples

      iex> create_subscription_changeset()
      %Ecto.Changeset{source: %Subscription{}}

  """
  @callback create_subscription_changeset(struct()) :: struct()
  @callback create_subscription_changeset() :: struct()
  def create_subscription_changeset(attrs \\ %{}) do
    Subscription.create_changeset(attrs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subscription changes.

  ## Examples

      iex> change_subscription(subscription)
      %Ecto.Changeset{source: %Subscription{}}

  """
  @callback change_subscription(struct()) :: struct()
  def change_subscription(%Subscription{} = subscription) do
    Subscription.update_changeset(subscription, %{})
  end

  @doc """
  Returns a list of invoices for a customer

  ## Examples

      iex> subscription = get_subscription!(1)
      iex> list_subscription_invoices!(subscription)
      [%{}]

  """
  @callback list_subscription_invoices!(string()) :: [struct()]
  def list_subscription_invoices!(subscription) do
    {:ok, stripe_invoices} = @stripe.list_customer_invoices(subscription.stripe_customer_id)
    stripe_invoices["data"]
    |> Enum.map(fn customer -> %CmcPayment.Payment.Invoice{
      id: customer["id"],
      amount_due: customer["amount_due"],
      amount_paid: customer["amount_paid"],
      amount_remaining: customer["amount_remaining"],
      created_at: DateTime.from_unix!(customer["created"]),
      pdf: customer["invoice_pdf"]
    }end)
  end

  alias CmcPayment.Payment.Charge

  @doc """
  Returns the list of charges.

  ## Examples

      iex> list_charges()
      [%Charge{}, ...]

  """
  def list_charges do
    Repo.all(Charge)
  end

  @doc """
  Gets a single charge.

  Raises `Ecto.NoResultsError` if the Charge does not exist.

  ## Examples

      iex> get_charge!(123)
      %Charge{}

      iex> get_charge!(456)
      ** (Ecto.NoResultsError)

  """
  def get_charge!(id), do: Repo.get!(Charge, id)

  @doc """
  Creates a charge.

  ## Examples

      iex> create_charge(%{field: value})
      {:ok, %Charge{}}

      iex> create_charge(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_charge(attrs \\ %{}) do
    %Charge{}
    |> Charge.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a charge.

  ## Examples

      iex> update_charge(charge, %{field: new_value})
      {:ok, %Charge{}}

      iex> update_charge(charge, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_charge(%Charge{} = charge, attrs) do
    charge
    |> Charge.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Charge.

  ## Examples

      iex> delete_charge(charge)
      {:ok, %Charge{}}

      iex> delete_charge(charge)
      {:error, %Ecto.Changeset{}}

  """
  def delete_charge(%Charge{} = charge) do
    Repo.delete(charge)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking charge changes.

  ## Examples

      iex> change_charge(charge)
      %Ecto.Changeset{source: %Charge{}}

  """
  def change_charge(%Charge{} = charge) do
    Charge.changeset(charge, %{})
  end
end
