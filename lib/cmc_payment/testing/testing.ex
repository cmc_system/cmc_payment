defmodule CmcPayment.Testing do
  @moduledoc """
  Manage test data on a non production environment.
  """

  import Ecto.Query, warn: false
  alias CmcPayment.Repo

  @doc """
  Create testing data.

  ## Examples

      iex> create!()
  """
  @callback create!() :: any()
  def create!() do
    path = :code.priv_dir(:cmc_payment)

    delete_all!
    Code.eval_file("#{path}/repo/seeds.exs")
  end

  @doc """
  Delete all testing data.

  ## Examples

      iex> delete_all!()
  """
  @callback delete_all!() :: any()
  def delete_all!() do
    Repo.query!("TRUNCATE subscriptions")
  end
end
