defmodule CmcPaymentWeb.Router do
  use CmcPaymentWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug BasicAuth, use_config: {:cmc_payment, :basic_auth}
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug BasicAuth, use_config: {:cmc_payment, :basic_auth}
    plug(JSONAPI.UnderscoreParameters)
  end

  pipeline :stats do
    plug :accepts, ["json"]
  end

  scope "/", CmcPaymentWeb  do
    pipe_through :stats

    resources "/health_check", HealthCheckController
  end

  scope "/", CmcPaymentWeb do
    pipe_through :browser # Use the default browser stack

    resources "/charges", ChargeController
    resources "/stripe_checkout", StripeCheckoutController
    resources "/", SubscriptionController
  end

  scope "/api", CmcPaymentWeb, as: :api do
    pipe_through :api

    scope "/v1", Api.V1, as: :v1 do
      resources "/stripe_session", StripeSessionController
      resources "/organizations", OrganizationController do
        resources "/subscription", SubscriptionController, singleton: true
        resources "/invoices", InvoiceController
      end
      resources "/subscriptions", SubscriptionController do
        resources "/invoices", InvoiceController
      end

      resources "/testing", TestingController, singleton: true
    end
  end
end
