defmodule CmcPaymentWeb.Api.V1.InvoiceView do
  use JSONAPI.View, type: "invoices", namespace: '/api/v1'
  alias CmcPaymentWeb.Api.V1.InvoiceView

  def render("index.json", %{invoices: invoices, conn: conn, params: params}) do
    InvoiceView.index(invoices, conn, params)
  end

  def fields do
    [:created_at, :amount_due, :amount_paid, :amount_remaining, :pdf]
  end
end
