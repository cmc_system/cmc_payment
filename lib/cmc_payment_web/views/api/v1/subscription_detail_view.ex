defmodule CmcPaymentWeb.Api.V1.SubscriptionDetailView do
  use JSONAPI.View, type: "subscriptions", namespace: '/api/v1'

  def fields do
    [
      :email, :organization_id, :stripe_session_id,
      :quantity, :iban_last4, :premia
    ]
  end
end
