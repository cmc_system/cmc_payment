defmodule CmcPaymentWeb.Api.V1.SubscriptionView do
  use JSONAPI.View, type: "subscriptions", namespace: '/api/v1'

  def fields do
    [
      :email, :organization_id,
      :stripe_session_id, :quantity
    ]
  end
end
