defmodule CmcPaymentWeb.Api.V1.StripeSessionView do
  use JSONAPI.View, type: "stripe_session", namespace: '/api/v1'

  def fields do
    []
  end
end
