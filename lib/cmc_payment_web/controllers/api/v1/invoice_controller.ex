defmodule CmcPaymentWeb.Api.V1.InvoiceController do
  use CmcPaymentWeb, :controller

  @payment Application.get_env(:cmc_payment, :payment)

  def index(conn, %{"subscription_id" => subscription_id} = params) do
    subscription = @payment.get_subscription!(subscription_id)
    invoices = @payment.list_subscription_invoices!(subscription)
    render(conn, "index.json", %{invoices: invoices, conn: conn, params: params})
  end

  def index(conn, %{"organization_id" => organization_id} = params) do
    subscription = @payment.get_subscription_by_organization!(organization_id)
    invoices = @payment.list_subscription_invoices!(subscription)
    render(conn, "index.json", %{invoices: invoices, conn: conn, params: params})
  end
end
