defmodule CmcPaymentWeb.Api.V1.SubscriptionController do
  use CmcPaymentWeb, :controller

  @payment Application.get_env(:cmc_payment, :payment)

  def show(conn, %{"id" => id}) do
    data = @payment.get_subscription!(id)
    render(conn, CmcPaymentWeb.Api.V1.SubscriptionDetailView, "show.json", %{data: data})
  end

  def show(conn, %{"organization_id" => organization_id}) do
    data = @payment.get_subscription_by_organization!(organization_id)
    render(conn, CmcPaymentWeb.Api.V1.SubscriptionDetailView, "show.json", %{data: data})
  end

  def create(conn, %{"data" => %{"attributes" => attributes}}) do
    case @payment.create_subscription(attributes) do
      {:ok, data} ->
        conn
        |> put_status(:created)
        |> render(CmcPaymentWeb.Api.V1.SubscriptionView, "show.json", %{data: data})
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(CmcPaymentWeb.ErrorView, "error.json", %{changeset: changeset})
      end
  end

  def update(conn, %{"id" => id, "data" => %{"attributes" => attributes}}) do
    subscription = @payment.get_subscription!(id)

    case @payment.update_subscription(subscription, attributes) do
      {:ok, subscription} ->
        conn
        |> put_status(:ok)
        |> render(CmcPaymentWeb.Api.V1.SubscriptionView, "show.json", %{data: subscription})
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(CmcPaymentWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end
end
