defmodule CmcPaymentWeb.Api.V1.TestingController do
  use CmcPaymentWeb, :controller

  @testing Application.get_env(:cmc_payment, :testing)

  def create(conn, _params) do
    @testing.create!

    conn
    |> put_status(:created)
    |> json(nil)
  end

  def delete(conn, _params) do
    @testing.delete_all!

    conn
    |> put_status(:ok)
    |> json(nil)
  end
end
