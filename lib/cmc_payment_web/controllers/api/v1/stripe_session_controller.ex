defmodule CmcPaymentWeb.Api.V1.StripeSessionController do
  use CmcPaymentWeb, :controller

  @payment Application.get_env(:cmc_payment, :payment)

  def create(conn, %{"data" => %{"attributes" => attributes}}) do
    case @payment.create_checkout_session(attributes) do
      {:ok, data} ->
         data = %{id: data["id"]}

         conn
         |> put_status(:created)
         |> render(CmcPaymentWeb.Api.V1.StripeSessionView, "show.json", %{data: data})
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(CmcPaymentWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end
end
