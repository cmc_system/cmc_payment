defmodule CmcPaymentWeb.StripeCheckoutController do
  use CmcPaymentWeb, :controller
  alias CmcPayment.Repo
  alias CmcPayment.Payment.Subscription

  def index(conn, %{"stripe_session_id" => stripe_session_id}) do
    render(conn, "index.html", stripe_session_id: stripe_session_id)
  end
end
