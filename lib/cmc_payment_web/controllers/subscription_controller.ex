defmodule CmcPaymentWeb.SubscriptionController do
  use CmcPaymentWeb, :controller

  alias CmcPayment.Payment
  alias CmcPayment.Payment.Subscription

  @payment Application.get_env(:cmc_payment, :payment)

  def index(conn, _params) do
    subscriptions = @payment.list_subscriptions()
    render(conn, "index.html", subscriptions: subscriptions)
  end

  def new(conn, _params) do
    changeset = @payment.create_subscription_changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"subscription" => subscription_params}) do
    case @payment.create_subscription(subscription_params) do
      {:ok, subscription} ->
        conn
        |> put_flash(:info, "Subscription created successfully.")
        |> redirect(to: subscription_path(conn, :show, subscription))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    subscription = @payment.get_subscription!(id)
    invoices = @payment.list_subscription_invoices!(subscription)
    render(conn, "show.html", subscription: subscription, invoices: invoices)
  end

  def edit(conn, %{"id" => id}) do
    subscription = @payment.get_subscription!(id)
    changeset = @payment.change_subscription(subscription)
    render(conn, "edit.html", subscription: subscription, changeset: changeset)
  end

  def update(conn, %{"id" => id, "subscription" => subscription_params}) do
    subscription = @payment.get_subscription!(id)

    case @payment.update_subscription(subscription, subscription_params) do
      {:ok, subscription} ->
        conn
        |> put_flash(:info, "Subscription updated successfully.")
        |> redirect(to: subscription_path(conn, :show, subscription))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", subscription: subscription, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    subscription = @payment.get_subscription!(id)
    {:ok, _subscription} = @payment.delete_subscription(subscription)

    conn
    |> put_flash(:info, "Subscription deleted successfully.")
    |> redirect(to: subscription_path(conn, :index))
  end
end
