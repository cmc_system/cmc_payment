defmodule CmcPaymentWeb.HealthCheckController do
  use CmcPaymentWeb, :controller
  alias CmcPayment.Repo
  alias CmcPayment.Payment.Subscription

  def index(conn, _params) do
    # check database ready
    Repo.one(Subscription)

    conn
    |> put_status(:ok)
    |> json(%{})
  end
end
